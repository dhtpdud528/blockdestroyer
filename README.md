# 본격! 다 부수는 게임

# 상점 페이지
[<img src="https://img.shields.io/badge/Google Play-414141?style=for-the-badge&logo=Google Play&logoColor=white">](https://play.google.com/store/apps/details?id=com.ec_h.buildingdestroyer&hl=ko)

# 개발 정보

### 개발 기간
2020/ 01/ 16 ~ 2020/ 02/ 19 출시

### 개발 툴, 언어

<img src="https://img.shields.io/badge/Unity-000000?style=for-the-badge&logo=Unity&logoColor=white">
<img src="https://img.shields.io/badge/C Sharp-239120?style=for-the-badge&logo=C Sharp&logoColor=white">

### 타겟 플랫폼
<img src="https://img.shields.io/badge/Android-3DDC84?style=for-the-badge&logo=Android&logoColor=white">

### 개발 인원
- **기획, 클라이언트 개발, 기타 UI및 아이콘 디자인 1명 (본인)**
- 그래픽 1명


---

# 게임 소개
### 장르
2D, 원터치 방치형 게임
### 스토리
- 인생의 과정을 하나의 게임으로 함축함
- 반도의 유명 건물들을 부수며, 성장

# 게임 특징
- 레트로 느낌의 캐주얼한 그래픽과 사운드
- 가볍게 즐길 수 있는 원터치 방치형 게임
- 알바(자동 진행 퀘스트) 시스템을 이용하여 게임내 기본 제화(코인) 습득 가능
- 장비를 장착하여, 플레이어 강화 가능
- 인앱 결제 시스템을 통한 기본 제화, 특수 제화(다이아), 아이템 습득 가능
- 광고 보기를 통한 특수 제화 습득 가능

# 플레이 영상
[![](http://img.youtube.com/vi/93n-kkRNEQk/0.jpg)](http://www.youtube.com/watch?v=93n-kkRNEQk "")

<!--
# 컨텐츠 설명
## 적
## 동료
## 플레이어
-->
