﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Numerics;
using Vector2 = UnityEngine.Vector2;

public class BreakableBlock : MonoBehaviour
{
    public Rigidbody2D m_rigidbody;
    public Collider2D m_collider;
    public SpriteRenderer sprite;
    public ParticleSystem damagedParticle;
    public ParticleSystem critParticle;
    public TextMeshPro textHp;

    public BigFloat maxHp;
    [SerializeField]
    BigFloat hp;
    public BigFloat Hp
    {
        get
        {
            return hp;
        }
        set
        {

            hp = value;
            if (hp > maxHp)
                hp = maxHp;

            if (GameManager.instance.boss != null && GameManager.instance.boss.Equals(this))
            {
                GameManager.instance.bossHPbar.fillAmount = float.Parse((hp / maxHp).ToString());
                GameManager.instance.bossHPText.text = GameManager.ConvertValue(hp) + " / " + GameManager.ConvertValue(maxHp);
            }
            else
                textHp.text = GameManager.ConvertValue(hp);

            if (hp <= 0)
                Break(true);

            
        }
    }
    

    public int blockLevel = 1;

    public void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody2D>();
        m_collider = GetComponent<Collider2D>();
        sprite = GetComponent<SpriteRenderer>();
    }
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (m_rigidbody.bodyType == RigidbodyType2D.Static) return;
        if (transform.position.y < 0 || transform.position.y > 8f || transform.position.x < -2.5f || transform.position.x > 2.5f)
        {
            float xPos = UnityEngine.Random.Range(-2f, GameManager.instance.maxSpawnPoint.x);
            transform.position = new Vector2(xPos, GameManager.instance.maxSpawnPoint.y);
            m_rigidbody.velocity *= 0;
        }
    }
    public void Damaged(BigFloat damage, bool isCritical)
    {
        if (hp <= 0) return;
        if(!GameManager.instance.audioSourceSFX.isPlaying)
            GameManager.instance.PlayRandomAudio_SFX(GameManager.instance.hitSounds);
        if (!isCritical)
            damagedParticle.Play();
        else
            critParticle.Play();
        Hp -= damage;
    }
    public void Ready()
    {
        GameManager.instance.breakedBlocks.Add(this);
        m_rigidbody.bodyType = RigidbodyType2D.Static;
        m_collider.enabled = false;
        transform.position = new Vector2(999, 999);
    }
    public void Break(bool isRecord)
    {
        hp = 0;
        damagedParticle.transform.parent.SetParent(null);
        GameManager.instance.PlayRandomAudio_SFX(GameManager.instance.breakSounds);
        if (isRecord)
        {
            GameManager.instance.playerController.profile.breakCount++;
            var dropedCoin = maxHp / 2;
            var addedCoin = dropedCoin * GameManager.instance.playerController.profile.coinMultiple;
            GameManager.instance.playerController.profile.Coins += dropedCoin + addedCoin;
            
            if (GameManager.instance.playerController.profile.MaxBlockLevel == blockLevel)
                GameManager.instance.playerController.profile.MaxBlockLevel++;

            if (GameManager.instance.boss != null && GameManager.instance.boss.Equals(this))
                GameManager.instance.playerController.profile.CurrentBlockLevel++;
        }

        GameManager.instance.SpawnCount--;

        if (GameManager.instance.boss == this)
        {
            GameManager.instance.boss = null;
            Destroy(gameObject);
        }
        else
        {
            GameManager.instance.spawnedBlocks.Remove(this);
            Ready();
        }
    }
    public void Spawn(bool isBoss)
    {
        
        damagedParticle.transform.parent.SetParent(transform);
        damagedParticle.transform.parent.localPosition = Vector2.zero;
        m_rigidbody.bodyType = RigidbodyType2D.Dynamic;
        
        this.blockLevel = GameManager.instance.playerController.profile.CurrentBlockLevel;


        if (isBoss)
        {

            this.maxHp = BigFloat.Pow(2, blockLevel + GameManager.instance.bossHpMutiply);
            float size = 1;
            transform.localScale = new Vector2(size, size);
            m_rigidbody.mass = 99999;
            m_rigidbody.drag = 2;
        }
        else
        {
            GameManager.instance.spawnedBlocks.Add(this);
            GameManager.instance.breakedBlocks.Remove(this);
            m_collider.enabled = true;
            this.maxHp = BigFloat.Pow(10, blockLevel);
            float size = 1;
            transform.localScale = new Vector2(size, size);
            m_rigidbody.mass = 1;
            m_rigidbody.drag = 0;
        }
        GameManager.instance.SpawnCount++;
        Hp = maxHp;
    }
}
