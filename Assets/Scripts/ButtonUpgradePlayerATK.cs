﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Numerics;
using Vector2 = UnityEngine.Vector2;

public class ButtonUpgradePlayerATK : UIPrice
{
    public Text upgradeCountText;
    public BigFloat addCoins;
    public BigFloat addValue = 1;
    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        addCoins = 100;
    }
    //public void OnUpgradeATK()
    //{
    //    if (GameManager.instance.playerController.profile.Coins < Price) return;
    //    //Debug.Log(GameManager.instance.playerController.profile.Coins+ " - "+needCoins);
    //    GameManager.instance.playerController.profile.Coins -= Price;
    //    GameManager.instance.playerController.profile.AttackDamage += addValue;
    //    Price += addCoins;
        
    //    GameManager.instance.playerController.profile.ATKUpgradeCount++;
    //    UpdateUI();
    //}
    public override void Buy()
    {
        base.Buy();
        GameManager.instance.playerController.profile.AttackDamage += addValue;
        Price += addCoins;

        GameManager.instance.playerController.profile.ATKUpgradeCount++;
        UpdateUI();
    }
    public void UpdateUI()
    {
        //if (!gameObject.activeSelf) return;

        if (GameManager.instance.playerController.profile.ATKLv > GameManager.instance.tap_Weapoons.weaponList.Count - 1)
        {
            GameManager.instance.playerController.weaponRenderer.sprite =
            GameManager.instance.tap_Weapoons.weaponList[GameManager.instance.tap_Weapoons.weaponList.Count - 1].body.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite;
        }
        else
        {
            GameManager.instance.playerController.weaponRenderer.sprite =
                GameManager.instance.tap_Weapoons.weaponList[GameManager.instance.playerController.profile.ATKLv].body.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite;
            GameManager.instance.tap_Weapoons.weaponList[GameManager.instance.playerController.profile.ATKLv].body.transform.GetChild(1).GetComponent<Text>().text =
        GameManager.instance.tap_Weapoons.weaponList[GameManager.instance.playerController.profile.ATKLv].weaponName + "\r\nATK: +" + GameManager.ConvertValue(addValue);

            if (GameManager.instance.playerController.profile.ATKUpgradeCount >= 5)
            {
                GameManager.instance.tap_Weapoons.weaponList[GameManager.instance.playerController.profile.ATKLv].body.transform.GetChild(1).GetComponent<Text>().text =
            GameManager.instance.tap_Weapoons.weaponList[GameManager.instance.playerController.profile.ATKLv].weaponName;
            }
            transform.position = GameManager.instance.tap_Weapoons.content.transform.GetChild(GameManager.instance.playerController.profile.ATKLv).GetChild(2).position;
            priceText.text = GameManager.ConvertValue(Price);
            upgradeCountText.text = "Upgrade \r\nLv. " + GameManager.instance.playerController.profile.ATKUpgradeCount + " / " + 5;
        }
        
    }
    
}
