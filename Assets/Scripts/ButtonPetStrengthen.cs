﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Numerics;
using Vector2 = UnityEngine.Vector2;
using Quaternion = UnityEngine.Quaternion;

public class ButtonPetStrengthen : MonoBehaviour
{
    public enum PetStrengthenType
    {
        AttackDamage,
        CriticalChance,
        CriticalMultiple,
        SplashRange,
    }
    public Color colorOrigin;
    public Text payText;
    public Text explainText;
    public BigFloat needCoins;
    public PetStrengthenType type;
    public float value;
    public BigFloat addValue = 0;
    // Start is called before the first frame update
    public void OnStrengthen()
    {
        if (GameManager.instance.playerController.profile.Coins >= needCoins)
        {
            //Debug.Log(GameManager.instance.playerController.profile.Coins+ " - "+needCoins);
            GameManager.instance.playerController.profile.Coins -= needCoins;
            //GameManager.instance.playerController.Strengthen(this, type, addValue, payText);
        }
    }
    private void Awake()
    {
        //Debug.Log(addValue +" = "+ new BigFloat(value));
        addValue = new BigFloat(value);
        //Debug.Log("결과 : "+addValue.ToString());
    }
    private void Start()
    {
        UpdatePrice();
    }
    public void UpdateAddValue()
    {
        string explain ="";
        //switch (type)
        //{
        //    case ButtonPlayerStrengthen.StrengthenType.AttackDamage:
        //        explain = "공격력 증가\r\n"+ addValue.ToString(1);
        //        break;
        //    case ButtonPlayerStrengthen.StrengthenType.CriticalMultiple:
        //        explain = "치명타 증가\r\n" + addValue.ToString(1);
        //        break;
        //    case ButtonPlayerStrengthen.StrengthenType.CriticalChance:
        //        explain = "치명타 확률 증가\r\n" + addValue.ToString(1);
        //        break;
        //    case ButtonPlayerStrengthen.StrengthenType.AutoAttackTime:
        //        explain = "자동 공격 속도 증가\r\n" + addValue.ToString(1);
        //        break;
        //    case ButtonPlayerStrengthen.StrengthenType.AutoSpawnTime:
        //        explain = "자동 블록 추가 속도 증가\r\n" + addValue.ToString(1);
        //        break;
        //    case ButtonPlayerStrengthen.StrengthenType.Bounciness:
        //        explain = "탄성 증가\r\n" + addValue.ToString(1);
        //        break;
        //    case ButtonPlayerStrengthen.StrengthenType.SplashRange:
        //        explain = "공격 범위 증가\r\n" + addValue.ToString(1);
        //        break;
        //    case ButtonPlayerStrengthen.StrengthenType.BlockSize:
        //        explain = "블록 사이즈 증가\r\n" + addValue.ToString(1);
        //        break;
        //}
        explainText.text = explain;
    }
    public void UpdateColor()
    {
        StartCoroutine(UpdateColorCoroutine());
    }
    IEnumerator UpdateColorCoroutine()
    {
        yield return new WaitForEndOfFrame();
        if (GameManager.instance.playerController.profile.Coins < needCoins)
            payText.color = Color.red;
        else
            payText.color = colorOrigin;
    }
    public void UpdatePrice()
    {
        GameManager.PlayerProfile profile = GameManager.instance.playerController.profile;
        BigFloat tempval = 0;
        needCoins = -1;
        //switch (type)
        //{
        //    case ButtonPlayerStrengthen.StrengthenType.AttackDamage:
        //        needCoins = (profile.AttackDamage * (tempval = 10));
        //        break;
        //    case ButtonPlayerStrengthen.StrengthenType.CriticalMultiple:
        //        needCoins = (profile.CriticalMultiple * (tempval = 100));
        //        break;
        //    case ButtonPlayerStrengthen.StrengthenType.CriticalChance:
        //        needCoins = profile.CriticalChance * 100;
        //        break;
        //    case ButtonPlayerStrengthen.StrengthenType.AutoAttackTime:
        //        if (profile.AutoAttackTime > 0)
        //            needCoins = 1000 / profile.AutoAttackTime;
        //        break;
        //    case ButtonPlayerStrengthen.StrengthenType.AutoSpawnTime:
        //        if (profile.AutoSpawnTime > 0)
        //            needCoins = 1000 / profile.AutoSpawnTime;
        //        break;
        //    case ButtonPlayerStrengthen.StrengthenType.Bounciness:
        //        needCoins = profile.Bounciness * 1000;
        //        break;
        //    case ButtonPlayerStrengthen.StrengthenType.SplashRange:
        //        needCoins = profile.SplashRange * 1000;
        //        break;
        //    case ButtonPlayerStrengthen.StrengthenType.BlockSize:
        //        needCoins = profile.BlockSize * 100;
        //        break;
        //}
        if (needCoins == -1)
            payText.text = "완료";
        else
            payText.text = GameManager.ConvertValue(needCoins);
        UpdateColor();
    }
}
