﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Numerics;
using Vector2 = UnityEngine.Vector2;
using Quaternion = UnityEngine.Quaternion;

public class PetController : MonoBehaviour
{
    public string petName;
    public Rigidbody2D m_rigidbody;
    public Rigidbody2D positionTrack;
    public Rigidbody2D rotationTrack;
    public Transform target;
    public GameManager.PetProfile profile;
    // Start is called before the first frame update
    private void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody2D>();
    }
    void Start()
    {
        profile.init();
    }

    // Update is called once per frame
    void Update()
    {
        //track.transform.position = GameManager.instance.playerController.transform.position;
        transform.position = target.position;
        rotationTrack.position = positionTrack.position;
    }
    private void FixedUpdate()
    {
        positionTrack.velocity = (Vector2)(GameManager.instance.playerController.transform.position - positionTrack.transform.position) * 500 * Time.deltaTime;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Breakable"))
        {
            var addedDamage = profile.AttackDamageMultiple * GameManager.instance.playerController.profile.AttackDamage / 2;
            if (UnityEngine.Random.value < (profile.CriticalChance / 100))
                collision.GetComponent<BreakableBlock>().Damaged(profile.CriticalMultiple * GameManager.instance.playerController.profile.AttackDamage / 2 + addedDamage, true);
            else
                collision.GetComponent<BreakableBlock>().Damaged(GameManager.instance.playerController.profile.AttackDamage / 2 + addedDamage, false);
        }
    }
}
