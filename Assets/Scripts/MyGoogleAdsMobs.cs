﻿using GoogleMobileAds.Api;
using System;
using UnityEngine;

public class MyGoogleAdsMobs : MonoBehaviour
{

    RewardedAd gem100ad;
    [SerializeField] string rewardUnitId;
    [SerializeField] bool isTest;
    void Start()
    {
        LoadAd();
    }

    void LoadAd()
    {
        if (gem100ad != null)
        {
            gem100ad.Destroy();
            gem100ad = null;
        }
        var adRequest = new AdRequest();

        // send the request to load the ad.
        RewardedAd.Load(rewardUnitId, adRequest,
            (RewardedAd ad, LoadAdError error) =>
            {
                // if error is not null, the load request failed.
                if (error != null || ad == null)
                {
                    Debug.LogError("하트 광고 불러오기 실패 " +
                                   "with error : " + error);
                    return;
                }

                gem100ad = ad;
                gem100ad.OnAdFullScreenContentClosed += () =>
                {
                    Debug.Log("OnAdClosed");
                    LoadAd();
                };

                Debug.Log("Rewarded ad loaded with response : "
                          + gem100ad.GetResponseInfo());
            });
    }
    public void OnBtnViewAdClicked()
    {
        const string rewardMsg =
        "Rewarded ad rewarded the user. Type: {0}, amount: {1}.";

        if (gem100ad != null && gem100ad.CanShowAd())
        {
            gem100ad.Show((Reward reward) =>
            {
                // TODO: Reward the user.
                Debug.Log("OnAdRewarded 100gem");
                GameManager.instance.playerController.profile.Gems += 100;
                GameManager.instance.GiveRewardDirection(GameManager.instance.gemSprite, 100);
                Debug.Log(String.Format(rewardMsg, reward.Type, reward.Amount));
            });
        }
    }
}
