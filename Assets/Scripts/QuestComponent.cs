﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Numerics;
using Vector2 = UnityEngine.Vector2;

public class QuestComponent : UIPrice
{
    public Image progressBar;
    public Text textReward;
    public Text textUpgradeCoin;
    public Text questName;
    public Text remainTimeText;
    BigFloat reward;
    BigFloat upgradeReward;
    public BigFloat Reward
    {
        get
        {
            return reward;
        }
        set
        {
            reward = value;
            textReward.text = GameManager.ConvertValue(value);
        }
    }
    public bool isPurchased;
    public float maxTime;
    public string rewardCoin;
    public float progressTime;

    public override void Awake()
    {
        base.Awake();
    }
    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        Price = float.Parse(priceString) <= 1 ? 1 : BigFloat.Parse(priceString) * (TimeMuchineManager.instance.timeTravelCount > 0 ? BigFloat.Pow(1000, TimeMuchineManager.instance.timeTravelCount) : 1);
        reward = BigFloat.Parse(rewardCoin) * (TimeMuchineManager.instance.timeTravelCount > 0 ? 1000 * TimeMuchineManager.instance.timeTravelCount : 1);
        upgradeReward = reward;
        textReward.text = GameManager.ConvertValue(reward);
        textUpgradeCoin.text = "+" + GameManager.ConvertValue(upgradeReward);

        float remainTime = maxTime - progressTime;
        remainTimeText.text = TimeCalculate(remainTime);
    }
    public string TimeCalculate(float time)
    {
        string result="";

        if (time / 86400 >= 1)
        {
            int day = (int)(time / 86400);
            result += day + "d";
            time -= day * 86400;
        }
        if (time / 3600 >= 1)
        {
            int hour = (int)(time / 3600);
            result += hour + "h";
            time -= hour * 3600;
        }
        if (time / 60 >= 1)
        {
            int min = (int)(time / 60);
            result += min + "m";
            time -= min * 60;
        }
        if (time > 0)
        {
            result += (int)time + "s";
        }
        return result;
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (!isPurchased) return;
        progressTime += Time.unscaledDeltaTime;
        remainTimeText.text = TimeCalculate(maxTime - progressTime);
        progressBar.fillAmount = progressTime / maxTime;
        if (progressBar.fillAmount >= 1)
        {
            UpdateColor();
            GameManager.instance.playerController.profile.Coins += reward;
            progressTime = 0;
        }
    }
    public void UpgradeQuestReward()
    {
        if (GameManager.instance.playerController.profile.Coins < Price) return;
        GameManager.instance.playerController.profile.Coins -= Price;
        if (!isPurchased)
            isPurchased = true;
        else
        {
            Reward += upgradeReward;
            if(Price <= 1)
            {
                Price = BigFloat.Parse(priceString) * (TimeMuchineManager.instance.timeTravelCount > 0 ? 1000 * TimeMuchineManager.instance.timeTravelCount : 1);
            }
            Price *= 2;
        }
    }
}
