﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMoveManager : MonoBehaviour
{
    public static SceneMoveManager instance;
    public float time;
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogWarning("Scene에 2개 이상의 장면전환 매니저가 존재합니다!");
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void MoveSecne(string secenName)
    {
        StartCoroutine(MoveSecneCoroutine(secenName));
    }
    IEnumerator MoveSecneCoroutine(string secenName)
    {
        yield return new WaitForSeconds(time);
        SceneManager.LoadScene(secenName);
    }
}
