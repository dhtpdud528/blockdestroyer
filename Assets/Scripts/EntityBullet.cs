﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityBullet : MonoBehaviour
{
    public Rigidbody2D m_rigidbody;
    Sprite weaponSprite;
    public Sprite WeaponSprite
    {
        get
        {
            return weaponSprite;
        }
        set
        {
            weaponSprite = value;
            GetComponent<SpriteRenderer>().sprite = value;
        }
    }
    public void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody2D>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Breakable"))
        {
            if (transform.position.x - collision.collider.transform.position.x < 0)
                transform.localScale = new Vector2(2, 2);
            else
                transform.localScale = new Vector2(-2, 2);
            var damagedDamage = GameManager.instance.playerController.profile.AttackDamage * GameManager.instance.playerController.profile.AttackDamageMultiple;
            if (UnityEngine.Random.value < GameManager.instance.playerController.profile.CriticalChance / 100)
                collision.collider.GetComponent<BreakableBlock>().Damaged(GameManager.instance.playerController.profile.CriticalMultiple * GameManager.instance.playerController.profile.AttackDamage + damagedDamage, true);
            else
                collision.collider.GetComponent<BreakableBlock>().Damaged(GameManager.instance.playerController.profile.AttackDamage + damagedDamage, false);

            if (collision.collider.GetComponent<BossCrackController>())
                collision.collider.GetComponent<BossCrackController>().MarkCracking(collision.contacts[0].point);
        }
        Destroy(gameObject);
    }
}
