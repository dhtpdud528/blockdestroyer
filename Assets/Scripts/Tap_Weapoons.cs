﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Numerics;
using Vector2 = UnityEngine.Vector2;
using System;

public class Tap_Weapoons : MonoBehaviour
{
    public GameObject weaponContent;
    public VerticalLayoutGroup content;
    public ButtonUpgradePlayerATK upgradePlayerATK;
    [Serializable]
    public class Weapon
    {
        public Sprite m_icon;
        public string weaponName;
        public GameObject body;
    }
    public List<Weapon> weaponList = new List<Weapon>();

    private void Awake()
    {
        for (int i = 0; i < weaponList.Count; i++)
        {
            GameObject weapon = Instantiate(weaponContent);

            weapon.transform.GetChild(1).GetComponent<Text>().text = weaponList[i].weaponName;
            weapon.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = weaponList[i].m_icon;
            weapon.transform.SetParent(content.transform);
            weapon.transform.localScale = new Vector2(1, 1);
            weaponList[i].body = weapon;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
        //upgradePlayerATK.UpdateUI();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
