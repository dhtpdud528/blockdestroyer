﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Numerics;

public enum ItemType
{
    Bomb, Coin, Gem
}
public class ButtonItem : UIPrice
{
    [Serializable]
    public class Product
    {

        public ItemType itemType;
        public string amount;
    }
    public Product[] products;
    public override void Buy()
    {
        
        base.Buy();
        for (int i = 0; i < products.Length; i++)
        {
            if (BigFloat.Parse(products[i].amount) == 0) continue;
            switch (products[i].itemType)
            {

                case ItemType.Bomb: GameManager.instance.playerController.profile.BombAmount += int.Parse(products[i].amount); break;
                case ItemType.Coin:
                    GameManager.instance.playerController.profile.Coins += BigFloat.Parse(products[i].amount);
                    GameManager.instance.GiveRewardDirection(GameManager.instance.coinSprite, BigFloat.Parse(products[i].amount));
                    break;
                case ItemType.Gem:
                    GameManager.instance.playerController.profile.Gems += BigFloat.Parse(products[i].amount);
                    GameManager.instance.GiveRewardDirection(GameManager.instance.gemSprite, BigFloat.Parse(products[i].amount));
                    break;
            }
        }
    }
}
