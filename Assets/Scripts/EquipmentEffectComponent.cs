﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Numerics;
using Vector2 = UnityEngine.Vector2;
using System;

public class EquipmentEffectComponent : MonoBehaviour
{
    public EquipmentSetComponent setContent;
    public GameManager.EffectType type;
    public string addValueString;
    public BigFloat addValue = 0;
    public Text effectText;
    public Toggle toggle;
    bool effectChecked;
    public bool EffectChecked
    {
        get
        {
            return effectChecked;
        }
        set
        {
            if (value && setContent.CanUseEffectCount <= 0) return;
            effectChecked = value;
            var val = addValue;
            if (effectChecked)
            {
                setContent.CanUseEffectCount--;
            }
            else
            {
                setContent.CanUseEffectCount++;
                val *= -1;
            }
            GameManager.instance.OnStrength(type, ref val);
        }
    }
    private void Awake()
    {
        addValue = BigFloat.Parse(addValueString);
        toggle.onValueChanged.AddListener((value) =>
        {
            EffectChecked = value;
        });
    }

    private void Start()
    {
        string explain = "";
        switch (type)
        {
            case GameManager.EffectType.CriticalMultiple:
                explain = "치명타\r\n" + (addValue > 1 ? "+" : "") + addValue.ToString(1)+"배";
                break;
            case GameManager.EffectType.CriticalChance:
                explain = "치명타 확률\r\n" + (addValue > 0 ? "+" : "") + addValue.ToString(1)+"%";
                break;
            case GameManager.EffectType.AutoAttackTime:
                explain = "자동 공격 시간\r\n" + (addValue > 0 ? "+" : "") + addValue.ToString(1)+"초";
                break;
            case GameManager.EffectType.AutoSpawnTime:
                explain = "자동 블록 추가 시간\r\n" + (addValue > 0 ? "+" : "") + addValue.ToString(1)+"초";
                break;
            case GameManager.EffectType.Bounciness:
                explain = "탄성\r\n" + (addValue > 0 ? "+" : "") + addValue.ToString(1);
                break;
            case GameManager.EffectType.MoreCoin:
                explain = "추가 코인 획득\r\n" + (addValue > 0 ? "+" : "") + (addValue*100).ToString(1)+ "%";
                break;
            case GameManager.EffectType.PetDamageMultiple:
                explain = "펫 추가 공격력\r\n" + (addValue > 0 ? "+" : "") + (addValue * 100).ToString(1) + "%";
                break;
            case GameManager.EffectType.PetCriticalChance:
                explain = "펫 치명타 확률\r\n" + (addValue > 0 ? "+" : "") + addValue.ToString(1) + "%";
                break;
            case GameManager.EffectType.PetCriticalMultiple:
                explain = "펫 치명타\r\n" + (addValue > 0 ? "+" : "") + addValue.ToString(1) + "배";
                break;
            case GameManager.EffectType.PetSpeed:
                explain = "펫 회전 속도\r\n" + (addValue > 0 ? "+" : "") + addValue.ToString(1);
                break;
            case GameManager.EffectType.AttackDamageMultiple:
                explain = "추가 공격력\r\n" + (addValue > 0 ? "+" : "") + (addValue * 100).ToString(1) + "%";
                break;
            case GameManager.EffectType.Wings:
                explain = "비행능력 추가";
                break;
        }
        effectText.text = explain;
    }
}
