﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using System;
using System.Numerics;
using Vector2 = UnityEngine.Vector2;
using Quaternion = UnityEngine.Quaternion;

public class PlayerController : MonoBehaviour
{
    Animator anim;
    public GameManager.PlayerProfile profile;
    public Rigidbody2D m_rigidbody;
    public BreakableBlock target;
    public GameObject bullet;
    public float jumpPower;
    public float throwPower;
    public float rotationSpeed;
    public float maxSpeed;
    public SpriteRenderer weaponRenderer;
    public IEnumerator autoAttack;
    public bool attackReady;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        m_rigidbody = GetComponent<Rigidbody2D>();

    }
    // Start is called before the first frame update
    void Start()
    {
        profile = new GameManager.PlayerProfile(GameManager.instance.defaultAutoAttackTime, GameManager.instance.defaultAutoSpawnTime, 1, 0, 1.1f, 0.1f, GameManager.instance.defaultCoins, GameManager.instance.defaultGems, 3);
        
        Init();
    }
    public void StopPlay()
    {
        StartCoroutine(StopPlayerCoroutine());
    }
    IEnumerator StopPlayerCoroutine()
    {
        GameManager.instance.playerController.m_rigidbody.gravityScale = 0;
        while (true)
        {
            GameManager.instance.playerController.m_rigidbody.velocity *= 0;
            yield return new WaitForEndOfFrame();
        }
    }
    public void Init()
    {
        StartCoroutine(InitCoroutine());
    }
    IEnumerator InitCoroutine()
    {
        yield return new WaitForEndOfFrame();
        if (!TimeMuchineManager.instance.isTimeTraveling)
            DataManager.instance.Load();
        if (autoAttack == null)
        {
            Debug.Log("초기값으로 덮어씌워짐");
            StartCoroutine(autoAttack = AutoAttackCoroutine());
        }
        GameManager.instance.Init();
        GameManager.instance.upgradeButtons.UpdateUI();
        GameManager.instance.textEquipStack.text = "착용한 코스튬: " + (GameManager.instance.playerController.profile.maxEquipStack - GameManager.instance.playerController.profile.equipStack) + "/<color=red><b>" + GameManager.instance.playerController.profile.maxEquipStack + "</b></color>";
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null && target.Hp <= 0)
            target = null;
        if (m_rigidbody.velocity.y < 0.1f && m_rigidbody.angularVelocity < 0.1f)
        {
            m_rigidbody.angularVelocity *= 0;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 0, 0), rotationSpeed * Time.deltaTime);
        }
        if (m_rigidbody.velocity.x > maxSpeed)
            m_rigidbody.velocity = new Vector2(maxSpeed, m_rigidbody.velocity.y);
        if (m_rigidbody.velocity.y > maxSpeed)
            m_rigidbody.velocity = new Vector2(m_rigidbody.velocity.x, maxSpeed);

        if (transform.position.y < 0 || transform.position.y > 8f || transform.position.x < -2.5f || transform.position.x > 2.5f)
            transform.position = new Vector2(0, 2.5f);

        if (target == null)
        {
            if(GameManager.instance.boss != null)
                target = GameManager.instance.boss;
            if (GameManager.instance.spawnedBlocks.Count > 0)
            {
                BreakableBlock block = null;
                BigFloat lowliest = BigFloat.Pow(2, profile.MaxBlockLevel + GameManager.instance.bossHpMutiply);
                for (int i = 0; i < GameManager.instance.spawnedBlocks.Count; i++)
                {
                    BigFloat hp = GameManager.instance.spawnedBlocks[i].Hp;
                    if (hp < lowliest)
                    {
                        block = GameManager.instance.spawnedBlocks[i];
                        lowliest = hp;
                    }
                }
                target = block;
            }
        }
    }
    public void Attack()
    {
        Vector2 touchPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (profile.Wings)
            m_rigidbody.velocity = (touchPoint - (Vector2)transform.position).normalized * jumpPower;
        var obj = Instantiate(bullet, transform.position, Quaternion.identity).GetComponent<EntityBullet>();
        obj.m_rigidbody.velocity = (touchPoint - (Vector2)transform.position).normalized * throwPower;
        obj.m_rigidbody.angularVelocity = UnityEngine.Random.Range(2000f, 5000f);
        obj.WeaponSprite = weaponRenderer.sprite;
        if (!GameManager.instance.audioSourceSFX.isPlaying)
            GameManager.instance.PlayOneShotRandomAudio_SFX(GameManager.instance.swingSounds);
        anim.CrossFadeInFixedTime("Attack", 0);
    }
    public void Attack(Vector2 position)
    {
        attackReady = true;
        if (GameManager.instance.touchEvent.isOnTouchSpace && Input.GetMouseButton(0))
            position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (profile.Wings)
            m_rigidbody.velocity = (position - (Vector2)transform.position).normalized * jumpPower;
        var obj = Instantiate(bullet, transform.position, Quaternion.identity).GetComponent<EntityBullet>();
        obj.m_rigidbody.velocity = (position - (Vector2)transform.position).normalized * throwPower;
        obj.m_rigidbody.angularVelocity = UnityEngine.Random.Range(2000f, 5000f);
        obj.WeaponSprite = weaponRenderer.sprite;
        if (!GameManager.instance.audioSourceSFX.isPlaying)
            GameManager.instance.PlayRandomAudio_SFX(GameManager.instance.swingSounds);
        anim.CrossFadeInFixedTime("Attack", 0);
    }
    public IEnumerator AutoAttackCoroutine()
    {
        while (true)
        {
            yield return new WaitUntil(() => target != null || Input.GetMouseButton(0));
            if (target != null)
                Attack(target.transform.position);
            else
                Attack();
            float time = 0;
            while (time < (profile.AutoAttackTime > 0 ? profile.AutoAttackTime : 0))
            {
                time += Time.deltaTime;
                GameManager.instance.autoAttackTimer.fillAmount = time / profile.AutoAttackTime;
                yield return new WaitForEndOfFrame();
            }
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (attackReady && collision.collider.CompareTag("Breakable"))
        {
            if (transform.position.x - collision.collider.transform.position.x < 0)
                transform.localScale = new Vector2(2, 2);
            else
                transform.localScale = new Vector2(-2, 2);
            var damagedDamage = profile.AttackDamage * profile.AttackDamageMultiple;
            if (UnityEngine.Random.value < profile.CriticalChance / 100)
                collision.collider.GetComponent<BreakableBlock>().Damaged(profile.CriticalMultiple * profile.AttackDamage + damagedDamage, true);
            else
                collision.collider.GetComponent<BreakableBlock>().Damaged(profile.AttackDamage + damagedDamage, false);
            anim.CrossFadeInFixedTime("Attack", 0);
            attackReady = false;

            if (collision.collider.GetComponent<BossCrackController>())
            {
                collision.collider.GetComponent<BossCrackController>().MarkCracking(collision.contacts[0].point);
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Breakable"))
        {
            if (transform.position.x - collision.collider.transform.position.x < 0)
                transform.localScale = new Vector2(2, 2);
            else
                transform.localScale = new Vector2(-2, 2);
            var damagedDamage = profile.AttackDamage * profile.AttackDamageMultiple;
            if (UnityEngine.Random.value < profile.CriticalChance / 100)
                collision.collider.GetComponent<BreakableBlock>().Damaged(profile.CriticalMultiple * profile.AttackDamage + damagedDamage, true);
            else
                collision.collider.GetComponent<BreakableBlock>().Damaged(profile.AttackDamage + damagedDamage, false);
            anim.CrossFadeInFixedTime("Attack", 0);

            if (collision.collider.GetComponent<BossCrackController>())
            {
                collision.collider.GetComponent<BossCrackController>().MarkCracking(collision.contacts[0].point);
            }
        }
    }
    public void UpdateProfile()
    {
        PhysicsMaterial2D temp = new PhysicsMaterial2D();
        temp.friction = 0;
        temp.bounciness = profile.Bounciness;
        GetComponent<Collider2D>().sharedMaterial = temp;
    }
}
