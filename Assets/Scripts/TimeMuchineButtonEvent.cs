﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeMuchineButtonEvent : MonoBehaviour
{
    private void Awake()
    {
        GetComponent<Button>().onClick.AddListener(() =>
        {
            FindObjectOfType<TimeMuchineManager>().DoTimeTravel();
        });
    }
}
