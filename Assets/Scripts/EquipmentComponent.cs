﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Numerics;
using Vector2 = UnityEngine.Vector2;
using System;
using DG.Tweening;

public class EquipmentComponent : UIPrice
{
    public int sortingOrderVale;
    public EquipmentSetComponent setContent;
    public Image equipImage;
    public GameObject equipOBJ;
    GameObject tempEquip;

    public Text equipName;
    [Header("- 버튼")]
    public Image ButtonCoinImage;

    bool isPurchased;
    public bool IsPurchased
    {
        get
        {
            return isPurchased;
        }
        set
        {
            if (value && GameManager.instance.playerController.profile.Gems < Price)
            {
                PopUpBoxManager.instance.RewardADBox.gameObject.SetActive(true);
                return;
            }
                
            isPurchased = value;
            GameManager.instance.playerController.profile.Gems -= Price;
            if (isPurchased)
            {
                ButtonCoinImage.gameObject.SetActive(false);
                priceText.text = "장착하기";
            }
        }
    }
    public void SetIsPurchaed(bool value)
    {
        isPurchased = value;
        if (isPurchased)
        {
            ButtonCoinImage.gameObject.SetActive(false);
            priceText.text = "장착하기";
        }
    }
    bool isEquip;
    public bool IsEquip
    {
        get
        {
            return isEquip;
        }
        set
        {
            if (!isPurchased) return;
            isEquip = value;
            if (isEquip)
            {
                if (GameManager.instance.playerController.profile.equipStack <= 0)
                {
                    isEquip = false;
                    if (GameManager.instance.tweeners != null && GameManager.instance.tweeners.IsPlaying())
                        GameManager.instance.tweeners.Complete();
                    GameManager.instance.tweeners = GameManager.instance.textEquipStack.rectTransform.DOPunchScale(new Vector2(0.1f, 0.1f), 1);
                    return;
                }
                priceText.text = "해재하기";
                setContent.CanUseEffectCount++;
                GameManager.instance.playerController.profile.equipStack--;

                var equipment = Instantiate(equipOBJ, GameManager.instance.transformEquipPosition);
                equipment.transform.localScale = new Vector2(1, 1);
                equipment.GetComponent<SpriteRenderer>().sprite = equipImage.sprite;
                
                equipment.GetComponent<SpriteRenderer>().sortingOrder = sortingOrderVale;
                if (sortingOrderVale != 0)
                    for (int i = 0; i < GameManager.instance.transformEquipPosition.childCount; i++)
                        GameManager.instance.transformEquipPosition.GetChild(i).GetComponent<SpriteRenderer>().sortingOrder = i;
                else
                    equipment.GetComponent<SpriteRenderer>().sortingLayerName = "Default";
                tempEquip = equipment;
                GameManager.instance.equips.Add(tempEquip);
                GameManager.instance.textEquipStack.text = "착용한 코스튬: " + (GameManager.instance.playerController.profile.maxEquipStack - GameManager.instance.playerController.profile.equipStack) + "/<color=red><b>" + GameManager.instance.playerController.profile.maxEquipStack+ "</b></color>";
            }
            else
            {
                priceText.text = "장착하기";
                for (int i = 0; i < setContent.effectComponents.Length; i++)
                    setContent.effectComponents[i].toggle.isOn = false;
                setContent.CanUseEffectCount--;
                GameManager.instance.playerController.profile.equipStack++;
                var equiped = GameManager.instance.equips.Find(sprite => sprite.GetComponent<SpriteRenderer>().sprite == tempEquip.GetComponent<SpriteRenderer>().sprite);
                GameManager.instance.equips.Remove(equiped);
                Destroy(equiped);
                GameManager.instance.textEquipStack.text = "착용한 코스튬: " + (GameManager.instance.playerController.profile.maxEquipStack - GameManager.instance.playerController.profile.equipStack) + "/<color=red><b>" + GameManager.instance.playerController.profile.maxEquipStack + "</b></color>";
            }
        }
    }
    public override void Awake()
    {
        base.Awake();
        priceText.text = GameManager.ConvertValue(Price);
    }
    public override void UpdateColor()
    {
        if (!isPurchased)
            base.UpdateColor();
    }
    public void OnButtonClick()
    {
        if (!isPurchased)
            IsPurchased = true;
        else
            IsEquip = !isEquip;
    }
}
