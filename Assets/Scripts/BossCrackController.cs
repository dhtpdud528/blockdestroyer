﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossCrackController : MonoBehaviour
{
    public GameObject crack;
    public Sprite[] crackSprites;
    // Start is called before the first frame update
    void Start()
    {
        if(GetComponent<SpriteRenderer>().sprite.name.Contains("사당"))
            transform.localScale = new Vector2(1.8f, 1.8f);
        else if (GetComponent<SpriteRenderer>().sprite.name.Contains("대학"))
            transform.localScale = new Vector2(0.65f, 0.65f);
        else if (GetComponent<SpriteRenderer>().sprite.name.Contains("요람"))
            transform.localScale = new Vector2(0.4f, 0.4f);
        else if (GetComponent<SpriteRenderer>().sprite.name.Contains("학사경"))
            transform.localScale = new Vector2(0.7f, 0.7f);
        else if (GetComponent<SpriteRenderer>().sprite.name.Contains("회사"))
            transform.localScale = new Vector2(0.55f, 0.55f);
        else if (GetComponent<SpriteRenderer>().sprite.name.Contains("대교"))
            transform.localScale = new Vector2(0.9f, 0.9f);
        else if (GetComponent<SpriteRenderer>().sprite.name.Contains("갓남"))
            transform.localScale = new Vector2(0.5f, 0.5f);
        GameManager.instance.bossNameText.text = GetComponent<SpriteRenderer>().sprite.name;
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnMouseDown()
    {
        Vector2 touchPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        MarkCracking(touchPoint);
        var damagedDamage = GameManager.instance.playerController.profile.AttackDamage * GameManager.instance.playerController.profile.AttackDamageMultiple;
        if (UnityEngine.Random.value < GameManager.instance.playerController.profile.CriticalChance / 100)
            GetComponent<BreakableBlock>().Damaged(GameManager.instance.playerController.profile.CriticalMultiple * GameManager.instance.playerController.profile.AttackDamage + damagedDamage, true);
        else
            GetComponent<BreakableBlock>().Damaged(GameManager.instance.playerController.profile.AttackDamage + damagedDamage, false);
    }
    public void MarkCracking(Vector2 point)
    {
        var crackobj = Instantiate(crack, point, Quaternion.Euler(0, 0, Random.Range(0, 360)));
        crackobj.transform.parent = transform;
        crackobj.GetComponent<SpriteRenderer>().sprite = crackSprites[Random.Range(0, crackSprites.Length)];
        GetComponent<SpriteMask>().sprite = GetComponent<SpriteRenderer>().sprite;
    }
}
