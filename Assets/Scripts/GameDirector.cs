﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    public GameObject ruleWindow;
    public Image fade;
    public CanvasGroup mainCanvasGroup;
    public CanvasGroup gameCanvasGroup;

    public GameObject[] tutorials;
    bool nextTut;
    public bool IsNextTut
    {
        get
        {
            return nextTut;
        }
        set
        {
            nextTut = value;
            if (nextTut)
                StartCoroutine(nextTutCoroutine());
        }
    }
    IEnumerator nextTutCoroutine()
    {
        yield return new WaitForEndOfFrame();
        nextTut = false;
    }
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartFade());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void GameStart()
    {
        StartCoroutine(GameStartCoroutine());
    }
    IEnumerator StartFade()
    {
        yield return new WaitForEndOfFrame();
        fade.DOFade(0, 2f);
    }
    IEnumerator GameStartCoroutine()
    {
        if (!DataManager.instance.IsRuleAgree)
        {
            ruleWindow.SetActive(true);
            yield return new WaitUntil(() => DataManager.instance.IsRuleAgree);
        }

        mainCanvasGroup.DOFade(0, 0.5f);
        gameCanvasGroup.DOFade(1, 0.5f);
        yield return new WaitForSeconds(0.5f);
        gameCanvasGroup.blocksRaycasts = true;
        mainCanvasGroup.blocksRaycasts = false;

        if (!GameManager.instance.playerController.profile.isDoneTut)
        {
            for (int i = 0; i < tutorials.Length; i++)
            {
                tutorials[i].SetActive(true);
                yield return new WaitUntil(() => nextTut);
                tutorials[i].SetActive(false);
                yield return new WaitForEndOfFrame();
            }
            GameManager.instance.playerController.profile.isDoneTut = true;
        }
    }
}
