﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using DG.Tweening;
using System.Numerics;
using Vector2 = UnityEngine.Vector2;
using Quaternion = UnityEngine.Quaternion;
using System.Text;
using AESWithJava.Con;

[Serializable]
public class EquipSetInfo
{
    [Serializable]
    public class EquipInfo
    {
        public string name;
        public bool isPuchase;
        public bool isEquip;

        public EquipInfo(string name, bool isPuchase, bool isEquip)
        {
            this.name = name;
            this.isPuchase = isPuchase;
            this.isEquip = isEquip;
        }
    }
    [Serializable]
    public class EquipEffectInfo
    {
        public string name;
        public bool isOn;

        public EquipEffectInfo(string name, bool isOn)
        {
            this.name = name;
            this.isOn = isOn;
        }
    }
    public string name;
    public EquipInfo[] equipInfos;
    public EquipEffectInfo[] equipEffectInfos;

    public EquipSetInfo(EquipmentSetComponent component)
    {
        this.name = component.setNameText.text;
        this.equipInfos = new EquipInfo[component.equipmentComponents.Length];
        for (int i = 0; i < component.equipmentComponents.Length; i++)
            this.equipInfos[i] = new EquipInfo(component.equipmentComponents[i].equipName.text, component.equipmentComponents[i].IsPurchased, component.equipmentComponents[i].IsEquip);

        this.equipEffectInfos = new EquipEffectInfo[component.effectComponents.Length];
        for (int i = 0; i < component.effectComponents.Length; i++)
            this.equipEffectInfos[i] = new EquipEffectInfo(component.effectComponents[i].name, component.effectComponents[i].toggle.isOn);
    }
}
[Serializable]
public class QuestInfo
{
    public string name;
    public bool isPuchased;
    public float progressTime;
    public string price;
    public string reward;

    public QuestInfo(QuestComponent component)
    {
        this.name = component.questName.text;
        this.isPuchased = component.isPurchased;
        this.progressTime = component.progressTime;
        this.price = component.Price.ToString();
        this.reward = component.Reward.ToString();
    }
}
[Serializable]
public class BurfInfo
{
    public string itemName;
    public float remainTime;

    public BurfInfo(ButtonBurfItem item)
    {
        this.itemName = item.itemName;
        this.remainTime = item.burfProfiles[0].remainTime;
    }
}

[Serializable]
public class WeaponInfo
{
    public string price;
    public string addValue;

    public WeaponInfo(ButtonUpgradePlayerATK playerATK)
    {
        this.price = playerATK.Price.ToString();
        this.addValue = playerATK.addValue.ToString();
    }
    public WeaponInfo()
    {
        this.price = "1";
        this.addValue = "1";
    }
}

[Serializable]
public class PlayerInfo
{
    public int bombAmount;
    public bool isDoneTut;
    public string gems;
    public string coins;
    public int atkLv;
    public int atkUpgradeCount;
    public string attckDamage;

    public int currentBlockLevel = 0;
    public int maxBlockLevel;
    public long breakCount;
    public int timeTravelCount;
    public int maxEquipStack;
    public int equipStack;

    public PlayerInfo(GameManager.PlayerProfile profile, bool isTimeTravel)
    {
        if (profile.Gems != 0)
            this.gems = profile.Gems.ToString();
        else
            this.gems = "0";

        bombAmount = profile.BombAmount;

        this.timeTravelCount = TimeMuchineManager.instance.timeTravelCount;
        this.equipStack = profile.maxEquipStack;
        this.maxEquipStack = profile.maxEquipStack + timeTravelCount;

        if (isTimeTravel)
        {
            this.isDoneTut = true;
            return;
        }


        this.isDoneTut = profile.isDoneTut;
        if (profile.Coins != 0)
            this.coins = profile.Coins.ToString();
        else
            this.coins = "0";
        this.atkLv = profile.ATKLv;
        this.atkUpgradeCount = profile.ATKUpgradeCount;
        this.currentBlockLevel = profile.CurrentBlockLevel;
        this.maxBlockLevel = profile.MaxBlockLevel;
        this.breakCount = profile.breakCount;
        this.attckDamage = profile.AttackDamage.ToString();
        
    }
}

[Serializable]
public class PetInfo
{
    public string name;
    public bool isPurchased;
    public bool isEquiped;

    public PetInfo(PetComponent component)
    {
        var pet = component.pet.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<PetController>();
        this.name = pet.petName;
        this.isPurchased = component.IsPurchased;
        this.isEquiped = component.IsEquip; ;
    }
}

[Serializable]
public class DataInfo
{
    public bool isRuleAgree;
    public List<BurfInfo> burfInfos = new List<BurfInfo>();
    public QuestInfo[] questInfos;
    public EquipSetInfo[] equipSetInfos;
    public PetInfo[] petInfos;
    public PlayerInfo playerInfo;
    public WeaponInfo weaponInfo;


    public DataInfo(GameManager gm, bool isTimeTravel)
    {
        if (isTimeTravel)
        {

            this.playerInfo = new PlayerInfo(gm.playerController.profile, true);
            this.petInfos = new PetInfo[gm.transformPetComponents.childCount];
            for (int i = 0; i < gm.transformPetComponents.childCount; i++)
            {
                petInfos[i] = new PetInfo(gm.transformPetComponents.GetChild(i).GetComponent<PetComponent>());
            }
            this.equipSetInfos = new EquipSetInfo[gm.transformEquipSetComponents.childCount];
            for (int i = 0; i < equipSetInfos.Length; i++)
            {
                equipSetInfos[i] = new EquipSetInfo(gm.transformEquipSetComponents.GetChild(i).GetComponent<EquipmentSetComponent>());
            }
            return;
        }
        this.playerInfo = new PlayerInfo(gm.playerController.profile, false);
        this.weaponInfo = new WeaponInfo(gm.tap_Weapoons.upgradePlayerATK);

        this.petInfos = new PetInfo[gm.transformPetComponents.childCount];
        for (int i = 0; i < gm.transformPetComponents.childCount; i++)
        {
            petInfos[i] = new PetInfo(gm.transformPetComponents.GetChild(i).GetComponent<PetComponent>());
        }

        for (int i = 0; i < gm.transformBurfItemComponents.childCount; i++)
        {
            if (!gm.transformBurfItemComponents.GetChild(i).GetComponent<ButtonBurfItem>()) continue;
            var temp = gm.transformBurfItemComponents.GetChild(i).GetComponent<ButtonBurfItem>();
            if (!temp.button.interactable)
                burfInfos.Add(new BurfInfo(gm.transformBurfItemComponents.GetChild(i).GetComponent<ButtonBurfItem>()));
        }
        this.questInfos = new QuestInfo[gm.transformQuestComponents.childCount];
        for (int i = 0; i < questInfos.Length; i++)
        {
            questInfos[i] = new QuestInfo(gm.transformQuestComponents.GetChild(i).GetComponent<QuestComponent>());
        }
        this.equipSetInfos = new EquipSetInfo[gm.transformEquipSetComponents.childCount];
        for (int i = 0; i < equipSetInfos.Length; i++)
        {
            equipSetInfos[i] = new EquipSetInfo(gm.transformEquipSetComponents.GetChild(i).GetComponent<EquipmentSetComponent>());
        }
    }
}
public class DataManager : MonoBehaviour
{
    bool isRuleAgree;
    public bool IsRuleAgree
    {
        get
        {
            return isRuleAgree;
        }
        set
        {
            isRuleAgree = value;
        }
    }
    public static DataManager instance;
    //펫 프로파일 제외? 하고 모든 프로파일 json저장 및 불러오기 구현할것

    public DataInfo dataInfo;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogWarning("Scene에 2개 이상의 데이터 매니저가 존재합니다!");
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Save(bool isTimeTravel)
    {
        if (!IsRuleAgree) return;
        dataInfo = new DataInfo(GameManager.instance, isTimeTravel);
        dataInfo.isRuleAgree = true;
        string JsonString = JsonUtility.ToJson(dataInfo);
        JsonString = "[" + JsonString + "]";
        //JsonString = JsonEncrypt.Encrypt(JsonString, "살려줘...");
        if (!Directory.Exists(Application.persistentDataPath + "/SaveData"))
            Directory.CreateDirectory(Application.persistentDataPath + "/SaveData");
        File.WriteAllText(Application.persistentDataPath + "/SaveData/BBData.json", JsonString);
    }
    public bool Load()
    {
        if (!File.Exists(Application.persistentDataPath + "/SaveData/BBData.json")) return false;

        string Jsonstring = //JsonEncrypt.Decrypt(
            File.ReadAllText(Application.persistentDataPath + "/SaveData/BBData.json");
            //, "살려줘...");

        //JsonData mapData = JsonMapper.ToObject(Jsonstring);
        JSONObject saveData = new JSONObject(Jsonstring);
        IsRuleAgree = saveData[0]["isRuleAgree"].b;
        if (true)
        {
            TimeMuchineManager.instance.timeTravelCount = (int)saveData[0]["playerInfo"]["timeTravelCount"].i;

            var damage = saveData[0]["playerInfo"]["attckDamage"].str == "" ? "1" : saveData[0]["playerInfo"]["attckDamage"].str;
            var coins = saveData[0]["playerInfo"]["coins"].str == "" ? "1" : saveData[0]["playerInfo"]["coins"].str;
            var gems = saveData[0]["playerInfo"]["gems"].str == "" ? "1" : saveData[0]["playerInfo"]["gems"].str;

            GameManager.instance.playerController.profile =
                new GameManager.PlayerProfile(GameManager.instance.defaultAutoAttackTime, GameManager.instance.defaultAutoSpawnTime, BigFloat.Parse(damage), 0, 1.1f, 0.1f, BigFloat.Parse(coins), BigFloat.Parse(gems), (int)saveData[0]["playerInfo"]["maxEquipStack"].i);
            GameManager.instance.playerController.profile.BombAmount = (int)saveData[0]["playerInfo"]["bombAmount"].i;
            GameManager.instance.playerController.profile.isDoneTut = saveData[0]["playerInfo"]["isDoneTut"].b;
            GameManager.instance.playerController.profile.ATKLv = (int)saveData[0]["playerInfo"]["atkLv"].i;
            GameManager.instance.playerController.profile.ATKUpgradeCount = (int)saveData[0]["playerInfo"]["atkUpgradeCount"].i;

            GameManager.instance.playerController.profile.MaxBlockLevel = (int)saveData[0]["playerInfo"]["maxBlockLevel"].i;
            GameManager.instance.playerController.profile.CurrentBlockLevel = (int)saveData[0]["playerInfo"]["currentBlockLevel"].i;

            GameManager.instance.playerController.profile.breakCount = (int)saveData[0]["playerInfo"]["breakCount"].i;
        }
        if (saveData[0]["weaponInfo"]["price"].str != "")
        {
            GameManager.instance.tap_Weapoons.upgradePlayerATK.Price = BigFloat.Parse(saveData[0]["weaponInfo"]["price"].str);
            GameManager.instance.tap_Weapoons.upgradePlayerATK.addValue = BigFloat.Parse(saveData[0]["weaponInfo"]["addValue"].str);
        }

        for (int i = 0; i < saveData[0]["petInfos"].Count; i++)
        {
            for (int a = 0; a < GameManager.instance.transformPetComponents.childCount; a++)
            {
                if (saveData[0]["petInfos"][i]["name"].str.Equals(GameManager.instance.transformPetComponents.GetChild(a).GetComponent<PetComponent>().pet.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<PetController>().petName))
                {
                    var pet = GameManager.instance.transformPetComponents.GetChild(a).GetComponent<PetComponent>();
                    pet.SetIsPurchaed(saveData[0]["petInfos"][i]["isPurchased"].b);
                    pet.IsEquip = saveData[0]["petInfos"][i]["isEquiped"].b;
                }
            }
        }
        for (int i = 0; i < saveData[0]["questInfos"].Count; i++)
        {
            for (int a = 0; a < GameManager.instance.transformQuestComponents.childCount; a++)
            {
                if (saveData[0]["questInfos"][i]["name"].str.Equals(GameManager.instance.transformQuestComponents.GetChild(a).GetComponent<QuestComponent>().questName.text))
                {
                    GameManager.instance.transformQuestComponents.GetChild(a).GetComponent<QuestComponent>().isPurchased = saveData[0]["questInfos"][i]["isPuchased"].b;
                    GameManager.instance.transformQuestComponents.GetChild(a).GetComponent<QuestComponent>().progressTime = saveData[0]["questInfos"][i]["progressTime"].f;
                    GameManager.instance.transformQuestComponents.GetChild(a).GetComponent<QuestComponent>().Price = BigFloat.Parse(saveData[0]["questInfos"][i]["price"].str);
                    GameManager.instance.transformQuestComponents.GetChild(a).GetComponent<QuestComponent>().Reward = BigFloat.Parse(saveData[0]["questInfos"][i]["reward"].str);
                }
            }
        }


        for (int setIndex = 0; setIndex < saveData[0]["equipSetInfos"].Count; setIndex++)
        {
            for (int a = 0; a < GameManager.instance.transformEquipSetComponents.childCount; a++)
            {
                if (saveData[0]["equipSetInfos"][setIndex]["name"].str.Equals(GameManager.instance.transformEquipSetComponents.GetChild(a).GetComponent<EquipmentSetComponent>().setNameText.text))
                {
                    for (int equipIndex = 0; equipIndex < saveData[0]["equipSetInfos"][setIndex]["equipInfos"].Count; equipIndex++)
                    {
                        if (saveData[0]["equipSetInfos"][setIndex]["equipInfos"][equipIndex]["name"].str.Equals(GameManager.instance.transformEquipSetComponents.GetChild(a).GetComponent<EquipmentSetComponent>().equipmentComponents[equipIndex].equipName.text))
                        {
                            if (saveData[0]["equipSetInfos"][setIndex]["equipInfos"][equipIndex]["isPuchase"].b)
                                GameManager.instance.transformEquipSetComponents.GetChild(a).GetComponent<EquipmentSetComponent>().equipmentComponents[equipIndex].SetIsPurchaed(saveData[0]["equipSetInfos"][setIndex]["equipInfos"][equipIndex]["isPuchase"].b);
                            if (saveData[0]["equipSetInfos"][setIndex]["equipInfos"][equipIndex]["isEquip"].b)
                                GameManager.instance.transformEquipSetComponents.GetChild(a).GetComponent<EquipmentSetComponent>().equipmentComponents[equipIndex].IsEquip = saveData[0]["equipSetInfos"][setIndex]["equipInfos"][equipIndex]["isEquip"].b;
                        }
                    }
                    for (int equipIndex = 0; equipIndex < saveData[0]["equipSetInfos"][setIndex]["equipEffectInfos"].Count; equipIndex++)
                    {
                        if (saveData[0]["equipSetInfos"][setIndex]["equipEffectInfos"][equipIndex]["name"].str.Equals(GameManager.instance.transformEquipSetComponents.GetChild(a).GetComponent<EquipmentSetComponent>().effectComponents[equipIndex].name))
                            if (saveData[0]["equipSetInfos"][setIndex]["equipEffectInfos"][equipIndex]["isOn"].b)
                                GameManager.instance.transformEquipSetComponents.GetChild(a).GetComponent<EquipmentSetComponent>().effectComponents[equipIndex].toggle.isOn = saveData[0]["equipSetInfos"][setIndex]["equipEffectInfos"][equipIndex]["isOn"].b;
                    }
                }
            }
        }

        for (int i = 0; i < saveData[0]["burfInfos"].Count; i++)
        {
            for (int a = 0; a < GameManager.instance.transformBurfItemComponents.childCount; a++)
            {
                if (GameManager.instance.transformBurfItemComponents.GetChild(a).GetComponent<ButtonBurfItem>() && GameManager.instance.transformBurfItemComponents.gameObject.activeSelf && saveData[0]["burfInfos"][i]["itemName"].str.Equals(GameManager.instance.transformBurfItemComponents.GetChild(a).GetComponent<ButtonBurfItem>().itemName))
                {
                    //Debug.Log(saveData[0]["burfInfos"][i]["itemName"].str);
                    GameManager.instance.transformBurfItemComponents.GetChild(a).GetComponent<ButtonBurfItem>().LoadBurf(saveData[0]["burfInfos"][i]["remainTime"].f);
                }
            }
        }
        return true;
    }
    public void DeleteSaveFile()
    {
        if (File.Exists(Application.persistentDataPath + "/SaveData/BBData.json"))
            File.Delete(Application.persistentDataPath + "/SaveData/BBData.json");
    }
    private void OnApplicationQuit()
    {
        if (!TimeMuchineManager.instance.isTimeTraveling)
            Save(false);
    }
    //public void OnApplicationFocus(bool value)
    //{
    //    if (!value && !TimeMuchineManager.instance.isTimeTraveling)
    //        Save(false);
    //}
    //public void OnApplicationPause(bool pause)
    //{
    //    if (pause && !TimeMuchineManager.instance.isTimeTraveling)
    //        Save(false);
    //}

}
