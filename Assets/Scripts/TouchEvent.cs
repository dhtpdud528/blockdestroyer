﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class TouchEvent : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
    public UnityEvent OnTouchDown;
    public bool isOnTouchSpace;
    public void OnPointerDown(PointerEventData eventData)
    {
        OnTouchDown.Invoke();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        isOnTouchSpace = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isOnTouchSpace = false;
    }
}
