﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MyEventHandler : MonoBehaviour
{
    public UnityEvent onEnable;
    public UnityEvent onUpdate;
    public UnityEvent onDisable;
    // Start is called before the first frame update

    private void OnEnable()
    {
        if (onEnable.GetPersistentEventCount() > 0)
            onEnable.Invoke();
    }
    // Update is called once per frame
    void Update()
    {
        if (onUpdate.GetPersistentEventCount() > 0)
            onUpdate.Invoke();
    }
    private void OnDisable()
    {
        if (onDisable.GetPersistentEventCount() > 0)
            onDisable.Invoke();
    }
}
