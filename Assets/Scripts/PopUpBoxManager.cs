﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpBoxManager : MonoBehaviour
{
    public static PopUpBoxManager instance;
    [SerializeField]
    private UI_PopUpBox ConfirmBox;
    public UI_PopUpBox RewardADBox;
    public UI_PopUpBox TimeTravelBox;
    public void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogWarning("Scene에 2개 이상의 팝업 매니저가 존재합니다!");
            Destroy(gameObject);
        }
    }
    public void PopConfirmBox(string val)
    {
        ConfirmBox.gameObject.SetActive(true);
        ConfirmBox.textBox.text = val;
    }
}
