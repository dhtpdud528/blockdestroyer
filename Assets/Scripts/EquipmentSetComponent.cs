﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Numerics;
using Vector2 = UnityEngine.Vector2;
using System;

public class EquipmentSetComponent : MonoBehaviour
{
    public Text setNameText;
    public EquipmentComponent[] equipmentComponents;
    public EquipmentEffectComponent[] effectComponents;
    int canUseEffectCount;
    public int CanUseEffectCount
    {
        get
        {
            return canUseEffectCount;
        }
        set
        {
            canUseEffectCount = value;
            if (canUseEffectCount <= 0)
            {
                for (int i = 0; i < effectComponents.Length; i++)
                    if (!effectComponents[i].toggle.isOn)
                        effectComponents[i].toggle.interactable = false;
                canUseEffectCount = 0;
            }
            else
                for (int i = 0; i < effectComponents.Length; i++)
                    effectComponents[i].toggle.interactable = true;
        }
    }
    public void RefreshEffect()
    {
        List<int> indexs = new List<int>();
        for (int i = 0; i < effectComponents.Length; i++)
        {
            if (effectComponents[i].toggle.isOn)
            {
                indexs.Add(i);
                effectComponents[i].toggle.isOn = false;
            }
        }
        for (int i = 0; i < indexs.Count; i++)
        {
            effectComponents[indexs[i]].toggle.isOn = true;
        }
    }
}
