﻿using System.Collections;
using System.Collections.Generic;
//using Cinemachine;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using DG.Tweening;
using System.Numerics;
using Vector2 = UnityEngine.Vector2;
using Quaternion = UnityEngine.Quaternion;
using System.Text;
using UnityEngine.Android;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    [Header("- 오디오")]
    public AudioSource audioSourceSFX;
    public AudioSource audioSourceBGM;

    public AudioClip[] mainBGMs;
    public AudioClip[] bossBGMs;

    public AudioClip[] bossDieSounds;
    public AudioClip winSound;
    public AudioClip failSound;
    public AudioClip[] bossSpawnSounds;
    public AudioClip blockSpawnSound;
    public AudioClip[] hitSounds;
    public AudioClip[] breakSounds;
    public AudioClip[] swingSounds;
    public AudioClip coinSound;
    public AudioClip gemSound;
    public AudioClip buttonClickSound;
    public AudioClip blackHoleOpenSound;
    public AudioClip timeTravelSound;
    public AudioClip exprosionSound;
    public AudioClip rewardSound;



    public enum EffectType
    {
        AutoAttackTime,
        AutoSpawnTime,
        AttackDamageMultiple,
        CriticalChance,
        CriticalMultiple,
        Bounciness,
        MoreCoin,
        PetDamageMultiple,
        PetCriticalChance,
        PetCriticalMultiple,
        PetSpeed,
        Wings,
        None
    }

    [Header("- 플레이어")]
    public float defaultAutoAttackTime;
    public float defaultAutoSpawnTime;
    public float defaultCoins;
    public float defaultGems;
    public float bombTime;

    public TouchEvent touchEvent;
    public PlayerController playerController;
    public PetController petController;
    public Transform petSpawnPosition;
    public MenuController menuController;

    [Serializable]
    public class PlayerProfile
    {
        int bombAmount;
        public int BombAmount
        {
            get
            {
                return bombAmount;
            }
            set
            {
                bombAmount = value;
                GameManager.instance.UpdateBombInfo();
            }
        }
        public bool isDoneTut;
        public long breakCount;
        int currentBlockLv;
        public int CurrentBlockLevel
        {
            get
            {
                return currentBlockLv;
            }
            set
            {
                currentBlockLv = value;
                GameManager.instance.currentBlockLevelText.text = "Lv." + currentBlockLv;
            }
        }
        int maxBlockLevel = 0;
        public int MaxBlockLevel
        {
            set
            {
                maxBlockLevel = value;
                //GameManager.instance.lvSlider.maxValue = maxBlockLevel;
            }
            get
            {
                return maxBlockLevel;
            }
        }
        int atkLv;
        public int ATKLv
        {
            get
            {
                return atkLv;
            }
            set
            {
                if (atkLv < value)
                    GameManager.instance.tap_Weapoons.upgradePlayerATK.addCoins *= 100;
                atkLv = value;
            }
        }
        int atkUpgradeCount;
        public int ATKUpgradeCount
        {
            get
            {
                return atkUpgradeCount;
            }
            set
            {
                atkUpgradeCount = value;
                GameManager.instance.upgradeButtons.addValue += 1;
                if (atkUpgradeCount >= 5)
                {
                    GameManager.instance.tap_Weapoons.upgradePlayerATK.UpdateUI();
                    ATKLv++;
                    
                    if (atkLv >= GameManager.instance.tap_Weapoons.content.transform.childCount)
                    {
                        GameManager.instance.tap_Weapoons.upgradePlayerATK.gameObject.SetActive(false);
                        return;
                    }
                    atkUpgradeCount = 0;
                }
            }
        }
        float autoAttackTime;
        public float AutoAttackTime
        {
            set
            {
                autoAttackTime = value;
                var tempTime = autoAttackTime;
                if (tempTime <= 0)
                    tempTime = 0;
                GameManager.instance.textAutoAttackTime.text = ": " + tempTime.ToString("N1") + "초";
            }
            get
            {
                return autoAttackTime;
            }
        }
        float autoSpawnTime;
        public float AutoSpawnTime
        {
            set
            {
                autoSpawnTime = value;
                var tempTime = autoSpawnTime;
                if (tempTime <= 0)
                    tempTime = 0;
                GameManager.instance.textAutoSpawnTime.text = ": " + tempTime.ToString("N1") + "초";
            }
            get
            {
                return autoSpawnTime;
            }
        }
        BigFloat attackDamage;
        public BigFloat AttackDamage
        {
            set
            {
                attackDamage = value;
                GameManager.instance.textDamage.text = ": " + ConvertValue(attackDamage + (attackDamage * attackDamageMultiple));

            }
            get
            {
                return attackDamage;
            }
        }
        float attackDamageMultiple;
        public float AttackDamageMultiple
        {
            get
            {
                return attackDamageMultiple;
            }
            set
            {
                attackDamageMultiple = value;
                GameManager.instance.textDamage.text = ": " + ConvertValue(attackDamage + (attackDamage * attackDamageMultiple));
            }
        }
        BigFloat criticalChance;
        public BigFloat CriticalChance
        {
            set
            {
                criticalChance = value;
                GameManager.instance.textCriticalChance.text = ConvertValue(value) + "%";
            }
            get
            {
                return criticalChance;
            }
        }
        BigFloat criticalMultiple;
        public BigFloat CriticalMultiple
        {
            set
            {
                criticalMultiple = value;
                GameManager.instance.textCritical.text = ": x" + ConvertValue(value);
            }
            get
            {
                return criticalMultiple;
            }
        }
        float bounciness;
        public float Bounciness
        {
            get
            {
                return bounciness;
            }
            set
            {
                bounciness = value;
                PhysicsMaterial2D temp = new PhysicsMaterial2D();
                temp.friction = GameManager.instance.playerController.GetComponent<Collider2D>().sharedMaterial.friction;
                temp.bounciness = bounciness;
                GameManager.instance.playerController.GetComponent<Collider2D>().sharedMaterial = temp;
            }
        }
        BigFloat coins;
        public BigFloat Coins
        {
            set
            {
                coins = value;
                GameManager.instance.textCoin.text = ": " + ConvertValue(value);
                //if (value > 0)
                //{
                //    if (GameManager.instance.tweeners != null && GameManager.instance.tweeners.IsPlaying())
                //        GameManager.instance.tweeners.Complete();
                //    GameManager.instance.tweeners = GameManager.instance.textCoin.rectTransform.DOPunchScale(new Vector2(0.1f, 0.1f), 1);
                //    //for (int i = 0; i < GameManager.instance.upgradeButtons.transform.childCount; i++)
                //    //    if(GameManager.instance.upgradeButtons.activeSelf)
                //    //        GameManager.instance.upgradeButtons.transform.GetChild(i).GetComponent<ButtonPlayerStrengthen>().UpdateColor();
                //}
            }
            get
            {
                return coins;
            }
        }

        BigFloat gems;
        public BigFloat Gems
        {
            set
            {
                gems = value;
                GameManager.instance.textGem.text = ": " + ConvertValue(value);
                if (value > 0)
                {
                    if (GameManager.instance.tweeners != null && GameManager.instance.tweeners.IsPlaying())
                        GameManager.instance.tweeners.Complete();
                    GameManager.instance.tweeners = GameManager.instance.textGem.rectTransform.DOPunchScale(new Vector2(0.1f, 0.1f), 1);
                    //for (int i = 0; i < GameManager.instance.upgradeButtons.transform.childCount; i++)
                    //    if(GameManager.instance.upgradeButtons.activeSelf)
                    //        GameManager.instance.upgradeButtons.transform.GetChild(i).GetComponent<ButtonPlayerStrengthen>().UpdateColor();
                }
            }
            get
            {
                return gems;
            }
        }
        public int maxEquipStack;
        public int equipStack;
        public float coinMultiple;
        bool wings;
        public bool Wings
        {
            get
            {
                return wings;
            }
            set
            {
                wings = value;
                if (wings)
                {
                    GameManager.instance.playerController.m_rigidbody.bodyType = RigidbodyType2D.Dynamic;
                    GameManager.instance.playerController.m_rigidbody.constraints = RigidbodyConstraints2D.None;
                    GameManager.instance.playerController.m_rigidbody.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
                    GameManager.instance.playerController.m_rigidbody.interpolation = RigidbodyInterpolation2D.Extrapolate;
                }
                else
                {
                    GameManager.instance.playerController.m_rigidbody.bodyType = RigidbodyType2D.Kinematic;
                    GameManager.instance.playerController.m_rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
                    GameManager.instance.playerController.m_rigidbody.collisionDetectionMode = CollisionDetectionMode2D.Discrete;
                    GameManager.instance.playerController.m_rigidbody.interpolation = RigidbodyInterpolation2D.None;
                    GameManager.instance.playerController.transform.DOMove(new Vector2(0, 0.42f),0.5f);
                }
            }
        }

        public PlayerProfile(float autoAttackTime, float autoSpawnTime, BigFloat attackDamage, float criticalChance, float criticalMultiple, float bounciness, BigFloat coins, BigFloat gems, int maxEquipStack)
        {
            this.AutoAttackTime = autoAttackTime;
            this.AutoSpawnTime = autoSpawnTime;
            this.AttackDamage = attackDamage;
            this.CriticalChance = criticalChance;
            this.CriticalMultiple = criticalMultiple;
            Bounciness = bounciness;
            this.Coins = coins;
            this.Gems = gems;
            this.AttackDamageMultiple = 0;
            this.coinMultiple = 0;
            this.equipStack = maxEquipStack;
            this.maxEquipStack = maxEquipStack;
        }
    }
    public PetProfile tempPetProfile;
    [Serializable]
    public class PetProfile
    {
        public EffectType type;
        [SerializeField]
        public float speed;
        [SerializeField]
        float attackDamageMultiple;
        public float AttackDamageMultiple
        {
            get
            {
                return attackDamageMultiple;
            }
            set
            {
                attackDamageMultiple = value;
            }
        }
        [SerializeField]
        float criticalChance;
        public float CriticalChance
        {
            set
            {
                criticalChance = value;
            }
            get
            {
                return criticalChance;
            }
        }
        [SerializeField]
        float criticalMultiple;

        public PetProfile(EffectType type, float speed, float attackDamageMultiple, float criticalChance, float criticalMultiple)
        {
            this.type = type;
            this.speed = speed;
            AttackDamageMultiple = attackDamageMultiple;
            CriticalChance = criticalChance;
            CriticalMultiple = criticalMultiple;
        }
        public PetProfile()
        {
            this.type = EffectType.None;
            speed = 0;
            AttackDamageMultiple = 0;
            CriticalChance = 0;
            CriticalMultiple = 0;
        }

        public float CriticalMultiple
        {
            set
            {
                criticalMultiple = value;
            }
            get
            {
                return criticalMultiple;
            }
        }
        public void init()
        {
            GameManager.instance.petController.rotationTrack.angularVelocity = speed * 100;
        }
        static PetProfile Add(PetProfile left, PetProfile right)
        {

            left.type = right.type;
            left.attackDamageMultiple += right.attackDamageMultiple;
            left.criticalChance += right.criticalChance;
            left.criticalMultiple += right.criticalMultiple;
            left.speed += right.speed;
            return left;
        }
        public static PetProfile operator +(PetProfile left, PetProfile right)
        {
            return Add(left, right);
        }
    }
    public List<GameObject> equips = new List<GameObject>();

    [Header("- UI & HUD")]
    public Transform burfIconPosition;
    public GameObject burfIcon;
    public Text textBombAmount;
    public Text currentBlockLevelText;
    public Text textCoin;
    public Text textGem;
    public Text textDamage;
    public Text textCritical;
    public Text textCriticalChance;
    public Text textAutoAttackTime;
    public Text textAutoSpawnTime;
    //public Slider lvSlider;
    public ButtonUpgradePlayerATK upgradeButtons;
    public Tap_Weapoons tap_Weapoons;
    public Transform transformEquipSetComponents;
    public Transform transformEquipPosition;
    public Transform transformQuestComponents;
    public Transform transformBurfItemComponents;
    public Transform transformPetComponents;
    public Text textEquipStack;
    public Image autoAttackTimer;
    public Image autoSpawnTimer;
    public Image fade;
    public Button buttonDynamite;

    public GameObject RewardDirection;
    public Sprite coinSprite;
    public Sprite gemSprite;
    public Image RewardIcon;
    public Text RewardText;

    public Button buttonTimeMuchine;
    public Text textTimer;

    public GameObject quitBox;

    [Header("- 환경 설정")]
    public Image buttonSoundIcon;
    public List<Sprite> buttonSoundSprites = new List<Sprite>();
    bool isSoundOn = true;
    public bool IsSoundOn
    {
        get
        {
            return isSoundOn;
        }
        set
        {
            isSoundOn = value;
            audioSourceSFX.mute = !isSoundOn;
            switch (isSoundOn)
            {
                case true: buttonSoundIcon.sprite = buttonSoundSprites.Find((val) => val.name.Contains("On")); break;
                case false: buttonSoundIcon.sprite = buttonSoundSprites.Find((val) => val.name.Contains("Off")); break;
            }
        }
    }
    
    public Image buttonBGMIcon;
    public List<Sprite> buttonBGMSprites = new List<Sprite>();
    bool isBGMOn = true;
    public bool IsBGMOn
    {
        get
        {
            return isBGMOn;
        }
        set
        {
            isBGMOn = value;
            audioSourceBGM.mute = !isBGMOn;
            switch (isBGMOn)
            {
                case true: buttonBGMIcon.sprite = buttonBGMSprites.Find((val) => val.name.Contains("On")); break;
                case false: buttonBGMIcon.sprite = buttonBGMSprites.Find((val) => val.name.Contains("Off")); break;
            }
        }
    }
    

    [Header("- 시간여행")]
    public GameObject timetravelEffect;

    [Header("- 블록")]
    public Vector2 maxSpawnPoint;
    public int maxSpawnCount;
    public int spawnCount;
    //[HideInInspector]
    public List<BreakableBlock> spawnedBlocks = new List<BreakableBlock>();
    //[HideInInspector]
    public List<BreakableBlock> breakedBlocks = new List<BreakableBlock>();
    public GameObject block;

    [Header("- 보스")]
    public int bossHpMutiply;
    public CanvasGroup bossComponets;
    public Image bossHPbar;
    public Text bossTimerText;
    public Text bossNameText;
    public Text bossHPText;
    public Sprite[] bossSprites;
    public GameObject bossOBJ;
    

    public BreakableBlock boss;
    IEnumerator autoBlock;
    public int SpawnCount
    {
        set
        {
            spawnCount = value;
        }
        get
        {
            return spawnCount;
        }
    }
    public Tweener tweeners;
    private Queue<IEnumerator> giveRewardCoroutine = new Queue<IEnumerator>();
    private IEnumerator currentGiveRewardCoroutine;

    public void UpdateBombInfo()
    {
        StartCoroutine(UpdateBombInfoCoroutine());
    }

    IEnumerator UpdateBombInfoCoroutine()
    {
        yield return new WaitForEndOfFrame();
        GameManager.instance.textBombAmount.text = playerController.profile.BombAmount.ToString();
    }
    public void ButtonSpawnBossBlock()
    {
        if (!audioSourceBGM.isPlaying || boss != null) return;

        int count = spawnCount;
        for (int i = 0; i < count; i++)
            spawnedBlocks[0].Break(true);
        SpawnNewBlock(true);
        PlayRandomAudio_BGM(bossBGMs);
        PlayOneShotRandomAudio_SFX(bossSpawnSounds);

        if (tweeners != null && tweeners.IsPlaying())
            tweeners.Complete();
        bossComponets.alpha = 1;
        playerController.transform.DOMoveX(-2, 1);
    }
    public void ButtonSpawnBlock()
    {
        audioSourceSFX.PlayOneShot(blockSpawnSound);
        if (SpawnCount < maxSpawnCount && boss == null)
            SpawnNewBlock(false);
        //bossComponets.alpha = 1;
    }
    IEnumerator BossTimer()
    {
        float time = 30;
        Color defaultColor = bossTimerText.color;
        Tweener tweenerColor = null;
        while (time > 0 && boss != null)
        {
            time -= Time.deltaTime;
            bossTimerText.text = time.ToString("N1") + "초";
            if(tweenerColor == null && time <= 10)
            tweenerColor = bossTimerText.DOColor(Color.red, time);
            yield return new WaitForEndOfFrame();
        }
        if(tweenerColor != null)
            tweenerColor.Complete();
        bossTimerText.color = defaultColor;
        
        if (time <= 0)
        {
            bossTimerText.text = "<color=red><b>실패</b></color>";
            audioSourceBGM.clip = failSound;
            audioSourceBGM.loop = false;
            audioSourceBGM.Play();
            tweeners = bossComponets.DOFade(0, 2);
            boss.Break(false);
        }
        else
        {
            bossTimerText.text = "<color=blue><b>성공</b></color>";
            audioSourceBGM.clip = winSound;
            audioSourceBGM.loop = false;
            audioSourceBGM.Play();
            PlayOneShotRandomAudio_SFX(bossDieSounds);
            tweeners = bossComponets.DOFade(0, 2);
        }
        yield return new WaitUntil(() => !audioSourceBGM.isPlaying);
        yield return new WaitForSeconds(1);
        playerController.transform.DOMoveX(0, 1);
        PlayRandomAudio_BGM(mainBGMs);
        audioSourceBGM.loop = true;
        audioSourceBGM.Play();
    }
    public void SpawnNewBlock(bool isBoss)
    {

        BreakableBlock spawnedBlock = null;

        if (isBoss)
        {
            spawnedBlock = Instantiate(bossOBJ, new Vector2(0, 0.5f), Quaternion.identity).GetComponent<BreakableBlock>();
            boss = spawnedBlock;
            spawnedBlock.Spawn(true);
            spawnedBlock.GetComponent<SpriteRenderer>().sprite = 
                bossSprites[playerController.profile.CurrentBlockLevel < bossSprites.Length ? playerController.profile.CurrentBlockLevel : UnityEngine.Random.Range(0, bossSprites.Length - 1)];
            spawnedBlock.gameObject.AddComponent<PolygonCollider2D>();
            //spawnedBlock.GetComponent<PixelPerfectCollider2D>().Regenerate();
            StartCoroutine(BossTimer());
        }
        else
        {
            float xPos = UnityEngine.Random.Range(-2f, maxSpawnPoint.x);
            spawnedBlock = breakedBlocks[UnityEngine.Random.Range(0, breakedBlocks.Count)];
            spawnedBlock.Spawn(false);
            spawnedBlock.transform.position = new Vector2(xPos, maxSpawnPoint.y);
        }
    }
    public void UseBomb()
    {
        

        if (playerController.profile.BombAmount <= 0) return;

        fade.color = new Color(1, 1, 1, 1);
        fade.DOFade(0, 0.5f);

        audioSourceSFX.PlayOneShot(exprosionSound);
        playerController.profile.BombAmount--;

        buttonDynamite.interactable = false;
        int count = spawnCount;
        
        if (boss != null)
            boss.Damaged(boss.maxHp/2,true);
        else
            for (int i = 0; i < count; i++)
                spawnedBlocks[0].Break(true);
        
        StartCoroutine(BombUseCoroutine());
    }
    IEnumerator BombUseCoroutine()
    {
        float timer = 0;
        while(timer < bombTime)
        {
            timer += Time.deltaTime;
            buttonDynamite.image.fillAmount = timer / bombTime;
            yield return new WaitForEndOfFrame();
        }
        buttonDynamite.interactable = true;
    }
    IEnumerator WaveBlocks()
    {
        yield return new WaitForEndOfFrame();
        while (true)
        {
            yield return new WaitUntil(() => breakedBlocks.Count > 0);
            SpawnNewBlock(false);
            float time = 0;
            while (time < (playerController.profile.AutoSpawnTime > 0 ? playerController.profile.AutoSpawnTime : 0))
            {
                time += Time.deltaTime;
                autoSpawnTimer.fillAmount = time / playerController.profile.AutoSpawnTime;
                yield return new WaitForEndOfFrame();
            }
            //yield return new WaitForSeconds(playerController.profile.AutoSpawnTime > 0 ? playerController.profile.AutoSpawnTime : 0);
            yield return new WaitUntil(() => SpawnCount < maxSpawnCount);
            if (boss != null && boss.Hp > 0)
                yield return new WaitUntil(() => boss == null && audioSourceBGM.loop);
        }
    }
    
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogWarning("Scene에 2개 이상의 게임 매니저가 존재합니다!");
            Destroy(gameObject);
        }
        tempPetProfile = new PetProfile();
    }
    // Start is called before the first frame update
    void Start()
    {
        if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite) || !Permission.HasUserAuthorizedPermission(Permission.ExternalStorageRead))
            RequestPermission();

        for (int i = 0; i < 100; i++)
        {
            BreakableBlock spawnedBlock = Instantiate(block).GetComponent<BreakableBlock>();
            spawnedBlock.Ready();
        }
    }
    public void Init()
    {
        if (autoBlock == null)
            StartCoroutine(autoBlock = WaveBlocks());
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0) && EventSystem.current.currentSelectedGameObject != null && EventSystem.current.currentSelectedGameObject.GetComponent<Button>())
            audioSourceSFX.PlayOneShot(buttonClickSound);
        if (giveRewardCoroutine.Count > 0 && currentGiveRewardCoroutine == null)
            StartCoroutine(currentGiveRewardCoroutine = giveRewardCoroutine.Dequeue());
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!quitBox.activeSelf)
                quitBox.SetActive(true);
            else
                Application.Quit();
        }
    }
    public static void Reverse(StringBuilder sb)
    {
        for (int i = 0, j = sb.Length - 1; i < sb.Length / 2; i++, j--)
        {
            char chT = sb[i];

            sb[i] = sb[j];
            sb[j] = chT;
        }
    }
    static string ToBase27AlphaString(int value)
    {
        StringBuilder sb = new StringBuilder();

        while (value > 0)
        {
            int digit = value % 26;
            sb.Append((char)('A' + digit));
            value /= 26;
        }

        if (sb.Length == 0)
        {
            sb.Append('A');
        }

        Reverse(sb);
        return sb.ToString();
    }
    public static string ConvertValue(BigFloat value)
    {

        BigFloat length = 0;
        string Alphabets = "";

        for (int i = 260; i >= 0; i--)
        {
            if (i == 0) return string.Format("{0}", value.ToString(1));
            if (value >= (length = BigFloat.Pow(new BigFloat(1000), i)))
            {
                Alphabets = ToBase27AlphaString(i - 1);
                break;
            }
        }
        return (value / length).ToString(1) + Alphabets;
    }
    public void PlayerBurf(Sprite sprite, ButtonBurfItem.BurfProfile[] burfProfiles, ButtonBurfItem item)
    {
        var icon = Instantiate(burfIcon, burfIconPosition);
        icon.transform.GetChild(0).GetComponent<Image>().sprite = sprite;
        item.button.interactable = false;

        for (int i = 0; i < burfProfiles.Length; i++)
            StartCoroutine(PlayerBurfCoroutine(sprite, burfProfiles[i], item, icon));
    }
    IEnumerator PlayerBurfCoroutine(Sprite sprite, ButtonBurfItem.BurfProfile burfProfile, ButtonBurfItem item, GameObject icon)
    {
        OnStrength(burfProfile.burfType, ref burfProfile.value);

        while (burfProfile.remainTime > 0)
        {
            burfProfile.remainTime -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        var temp = -burfProfile.value;
        OnStrength(burfProfile.burfType, ref temp);

        Destroy(icon);
        item.button.interactable = true;
    }
    public void OnStrength(EffectType type, ref BigFloat val)
    {
        switch (type)
        {
            case GameManager.EffectType.AttackDamageMultiple:
                playerController.profile.AttackDamageMultiple += float.Parse(val.ToString());
                break;
            case GameManager.EffectType.CriticalMultiple:
                playerController.profile.CriticalMultiple += val;
                break;
            case GameManager.EffectType.CriticalChance:
                playerController.profile.CriticalChance += val;
                break;

            case GameManager.EffectType.AutoAttackTime:

                playerController.profile.AutoAttackTime += float.Parse(val.ToString());
                if (GameManager.instance.playerController.autoAttack != null)
                    StopCoroutine(GameManager.instance.playerController.autoAttack);

                StartCoroutine(GameManager.instance.playerController.autoAttack = GameManager.instance.playerController.AutoAttackCoroutine());
                break;

            case GameManager.EffectType.AutoSpawnTime:
                if(val == 0)
                    playerController.profile.AutoSpawnTime += 0;
                else
                playerController.profile.AutoSpawnTime += float.Parse(val.ToString());
                if (autoBlock != null)
                    StopCoroutine(autoBlock);

                StartCoroutine(autoBlock = WaveBlocks());
                break;
            case GameManager.EffectType.Bounciness:
                playerController.profile.Bounciness += float.Parse(val.ToString());
                break;
            case GameManager.EffectType.MoreCoin:
                playerController.profile.coinMultiple += float.Parse(val.ToString());
                break;
            case GameManager.EffectType.PetDamageMultiple:
                if (petController != null)
                {
                    tempPetProfile.AttackDamageMultiple += float.Parse(val.ToString());
                    petController.profile.AttackDamageMultiple += float.Parse(val.ToString());
                }
                break;
            case GameManager.EffectType.PetCriticalChance:
                if (petController != null)
                {
                    tempPetProfile.CriticalChance += float.Parse(val.ToString());
                    petController.profile.CriticalChance += float.Parse(val.ToString());
                }
                break;
            case GameManager.EffectType.PetCriticalMultiple:
                if (petController != null)
                {
                    tempPetProfile.CriticalMultiple += float.Parse(val.ToString());
                    petController.profile.CriticalMultiple += float.Parse(val.ToString());
                }
                break;
            case GameManager.EffectType.PetSpeed:
                if (petController != null)
                {
                    tempPetProfile.speed += float.Parse(val.ToString());
                    petController.profile.speed += float.Parse(val.ToString());
                    petController.profile.init();
                }
                break;
            case GameManager.EffectType.Wings:
                playerController.profile.Wings = int.Parse(val.ToString()) > 0 ? true : false;
                break;
        }
    }
    public void SwitchSound()
    {
        IsSoundOn = !IsSoundOn;
    }
    public void SwitchBGM()
    {
        IsBGMOn = !IsBGMOn;
    }

    public void PlayOneShotRandomAudio_BGM(AudioClip[] audios)
    {
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = UnityEngine.Random.Range(1, audios.Length);
        audioSourceBGM.clip = audios[n];
        audioSourceBGM.PlayOneShot(audioSourceBGM.clip);
        // move picked sound to index 0 so it's not picked next time
        audios[n] = audios[0];
        audios[0] = audioSourceBGM.clip;
    }
    public void PlayRandomAudio_BGM(AudioClip[] audios)
    {
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = UnityEngine.Random.Range(1, audios.Length);
        audioSourceBGM.clip = audios[n];
        audioSourceBGM.Play();
        // move picked sound to index 0 so it's not picked next time
        audios[n] = audios[0];
        audios[0] = audioSourceBGM.clip;
    }
    public void PlayRandomAudio_SFX(AudioClip[] audios)
    {
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = UnityEngine.Random.Range(1, audios.Length);
        audioSourceSFX.clip = audios[n];
        audioSourceSFX.Play();
        // move picked sound to index 0 so it's not picked next time
        audios[n] = audios[0];
        audios[0] = audioSourceSFX.clip;
    }
    public void PlayOneShotRandomAudio_SFX(AudioClip[] audios)
    {
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = UnityEngine.Random.Range(1, audios.Length);
        audioSourceSFX.clip = audios[n];
        audioSourceSFX.PlayOneShot(audioSourceSFX.clip);
        // move picked sound to index 0 so it's not picked next time
        audios[n] = audios[0];
        audios[0] = audioSourceSFX.clip;
    }

    public void GiveRewardDirection(Sprite icon, BigFloat amount)
    {
        giveRewardCoroutine.Enqueue(GiveRewardDirectionCoroutine(icon, amount));
    }
    IEnumerator GiveRewardDirectionCoroutine(Sprite icon, BigFloat amount)
    {
        audioSourceSFX.PlayOneShot(rewardSound);
        RewardDirection.SetActive(true);
        RewardIcon.sprite = icon;
        RewardText.text = ConvertValue(amount);
        RewardDirection.transform.DOScale(new Vector2(1,1),0.5f);
        yield return new WaitForSeconds(2);
        RewardDirection.transform.DOScale(new Vector2(0, 0), 0.5f);
        yield return new WaitForSeconds(0.5f);
        RewardDirection.SetActive(false);
        currentGiveRewardCoroutine = null;
    }
    public void OpenInputURL(InputField inputField)
    {
        Application.OpenURL(inputField.text);
    }
    public void OpenRule()
    {
        Application.OpenURL("https://drive.google.com/open?id=1odB3QYqnGdwjl-tGqAbXanR21493uizi");
    }
    public void ChooseRuleAgree(bool val)
    {
        if (!val)
            Application.Quit();
    }
    public void RequestPermission()
    {
        Permission.RequestUserPermission(Permission.ExternalStorageWrite);
        Permission.RequestUserPermission(Permission.ExternalStorageRead);
    }

    private void OnAllow()
    {
        // execute action that uses permitted function.
    }

    private void OnDeny()
    {
        PopUpBoxManager.instance.PopConfirmBox("게임저장 권한이 거부되었습니다.\r\n 게임이 저장되지 않습니다.");
        // back screen / show warnking window
    }

    private void OnDenyAndNeverAskAgain()
    {
        PopUpBoxManager.instance.PopConfirmBox("게임저장 권한이 거부되었습니다.\r\n 게임이 저장되지 않습니다.");
        // show warning window and open app permission setting page
    }
}

