﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Numerics;
using Vector2 = UnityEngine.Vector2;
using Quaternion = UnityEngine.Quaternion;

public class ButtonBurfItem : UIPrice
{
    [Serializable]
    public class BurfProfile
    {
        public GameManager.EffectType burfType;
        public float valueInspector;
        public BigFloat value;
        public float remainTime;
        public BurfProfile(BurfProfile copy)
        {
            burfType = copy.burfType;
            value = copy.value;
            remainTime = copy.remainTime;
        }
    }
    public string itemName;
    public Image icon;
    public Button button;
    public BurfProfile[] burfProfiles;
    public float burfTime;
    public override void Awake()
    {
        base.Awake();
        for (int i = 0; i < burfProfiles.Length; i++)
        {
            burfProfiles[i].value = burfProfiles[i].valueInspector;
            burfProfiles[i].remainTime = burfTime;
        }

    }
    // Start is called before the first frame update
    //public void OnBurf()
    //{
    //    switch (paymentType)
    //    {
    //        case PaymentType.Coin:
    //            if (GameManager.instance.playerController.profile.Coins >= Price)
    //            {
    //                //BurfProfile[] tmepProfiles = new BurfProfile[burfProfiles.Length];
    //                //for (int i = 0; i < tmepProfiles.Length; i++)
    //                //    tmepProfiles[i] = new BurfProfile(burfProfiles[i]);
    //                GameManager.instance.playerController.profile.Coins -= Price;
    //                GameManager.instance.PlayerBurf(icon.sprite, burfProfiles, this);
    //            }
    //            break;
    //        case PaymentType.Gem:
    //            if (GameManager.instance.playerController.profile.Gems >= Price)
    //            {
    //                //BurfProfile[] tmepProfiles = new BurfProfile[burfProfiles.Length];
    //                //for (int i = 0; i < tmepProfiles.Length; i++)
    //                //    tmepProfiles[i] = new BurfProfile(burfProfiles[i]);
    //                GameManager.instance.playerController.profile.Gems -= Price;
    //                GameManager.instance.PlayerBurf(icon.sprite, burfProfiles, this);
    //            }

    //            break;
    //    }
    //}
    public override void Buy()
    {
        base.Buy();
        for (int i = 0; i < burfProfiles.Length; i++)
            burfProfiles[i].remainTime = burfTime;
        GameManager.instance.PlayerBurf(icon.sprite, burfProfiles, this);

    }
    public void LoadBurf(float remainTime)
    {
        //BurfProfile[] tmepProfiles = new BurfProfile[burfProfiles.Length];
        //for (int i = 0; i < tmepProfiles.Length; i++)
        //{
        //    tmepProfiles[i] = new BurfProfile(burfProfiles[i]);
        //    tmepProfiles[i].remainTime = remainTime;
        //}
        for (int i = 0; i < burfProfiles.Length; i++)
            burfProfiles[i].remainTime = remainTime;
        GameManager.instance.PlayerBurf(icon.sprite, burfProfiles, this);
    }
}
