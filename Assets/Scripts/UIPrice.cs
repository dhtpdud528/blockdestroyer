﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;
using Vector2 = UnityEngine.Vector2;
using System;

public class UIPrice : MonoBehaviour
{
    public string priceString;
    public enum PaymentType { Coin, Gem }
    public PaymentType paymentType;
    BigFloat capital = 0;
    BigFloat price;
    public BigFloat Price
    {
        get
        {
            return price;
        }
        set
        {
            price = value;
            if (priceText != null)
                priceText.text = GameManager.ConvertValue(value);
        }
    }
    public Text priceText;
    public Color colorOrigin;
    public BigFloat tempCoin;
    // Start is called before the first frame update
    public virtual void Awake()
    {
        Price = BigFloat.Parse(priceString);
        tempCoin = -1;
        //priceText.text = GameManager.ConvertValue(Price);
    }
    public virtual void Start()
    {
        
    }
    // Update is called once per frame
    public virtual void Update()
    {
        switch (paymentType)
        {
            case PaymentType.Coin:
                capital = GameManager.instance.playerController.profile.Coins;
                break;
            case PaymentType.Gem:
                capital = GameManager.instance.playerController.profile.Gems;
                break;
        }
        if (capital != tempCoin)
        {
            if (GameManager.instance.menuController.transform.parent.localPosition.y > 10)
                UpdateColor();
            tempCoin = capital;
        }

    }
    public virtual void UpdateColor()
    {
        StartCoroutine(UpdateColorCoroutine());
    }
    IEnumerator UpdateColorCoroutine()
    {
        yield return new WaitForEndOfFrame();
        if (priceText != null)
            if (capital < Price)
                priceText.color = Color.red;
            else
                priceText.color = colorOrigin;
    }
    public virtual void TryToPay()
    {
        switch (paymentType)
        {
            case PaymentType.Coin:
                if (GameManager.instance.playerController.profile.Coins >= Price)
                {
                    //BurfProfile[] tmepProfiles = new BurfProfile[burfProfiles.Length];
                    //for (int i = 0; i < tmepProfiles.Length; i++)
                    //    tmepProfiles[i] = new BurfProfile(burfProfiles[i]);
                    GameManager.instance.playerController.profile.Coins -= Price;
                    Buy();
                }
                break;
            case PaymentType.Gem:
                if (GameManager.instance.playerController.profile.Gems >= Price)
                {
                    //BurfProfile[] tmepProfiles = new BurfProfile[burfProfiles.Length];
                    //for (int i = 0; i < tmepProfiles.Length; i++)
                    //    tmepProfiles[i] = new BurfProfile(burfProfiles[i]);
                    GameManager.instance.playerController.profile.Gems -= Price;
                    Buy();
                }
                else
                    PopUpBoxManager.instance.RewardADBox.gameObject.SetActive(true);
                break;
        }
    }
    public virtual void Buy()
    {
        UpdateColor();

        switch (paymentType)
        {
            case PaymentType.Coin: GameManager.instance.audioSourceSFX.PlayOneShot(GameManager.instance.coinSound); break;
            case PaymentType.Gem: GameManager.instance.audioSourceSFX.PlayOneShot(GameManager.instance.gemSound); break;
        }
    }
}
