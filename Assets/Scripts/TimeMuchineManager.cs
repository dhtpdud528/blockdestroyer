﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
using System.IO;
using System;
using AESWithJava.Con;

public class TimeMuchineManager : MonoBehaviour
{
    public static TimeMuchineManager instance;
    public int timeTravelCount;
    [SerializeField]
    int isReadyTimeTravel;
    public bool isTimeTraveling;
    public int IsReadyTimeTravel
    {
        get
        {
            return isReadyTimeTravel;
        }
        set
        {
            isReadyTimeTravel = value;
            Debug.Log("값 변경" + IsReadyTimeTravel);
            if (isReadyTimeTravel <= 0)
                GameManager.instance.buttonTimeMuchine.interactable = false;
            else
            {
                GameManager.instance.buttonTimeMuchine.interactable = true;
                GameManager.instance.textTimer.text = "";
            }
        }
    }
    
    // Start is called before the first frame update

    private DateTime m_AppQuitTime = new DateTime(1970, 1, 1).ToLocalTime();
    private const int MAX_HEART = 1;
    public int HeartSupplyInterval = 86400;
    public int remainHeartSupplyTime;
    private Coroutine supplyTimerCoroutine = null;

    public void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogWarning("Scene에 2개 이상의 타임머신이 존재합니다!");
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        Init();
        //timeTravelCount = GameManager.instance.playerController.profile.maxEquipStack - 3;
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void DoTimeTravel()
    {
        if (GameManager.instance.playerController.profile.CurrentBlockLevel <= 3 + timeTravelCount * 3)
        {
            PopUpBoxManager.instance.PopConfirmBox("시간여행은 <color=red><b>Lv." + (3 + timeTravelCount * 3) + "철거성공 이후</b></color> 가능합니다.");
            return;
        }
        GameManager.instance.menuController.IsOn = false;
        GameManager.instance.timetravelEffect.SetActive(true);
        GameManager.instance.timetravelEffect.transform.DOScale(new UnityEngine.Vector2(13, 13), 0.1f);
        GameManager.instance.audioSourceSFX.PlayOneShot(GameManager.instance.blackHoleOpenSound);
        GameManager.instance.playerController.StopPlay();
        StartCoroutine(TimeTravelDirect());

    }
    IEnumerator TimeTravelDirect()
    {
        yield return new WaitForSeconds(2);

        GameManager.instance.audioSourceSFX.PlayOneShot(GameManager.instance.timeTravelSound);
        GameManager.instance.playerController.transform.DOMove(GameManager.instance.timetravelEffect.transform.position, 0.5f);
        GameManager.instance.fade.DOFade(1, 0.5f);
        yield return new WaitForSeconds(2);

        timeTravelCount++;
        isTimeTraveling = true;
        DataManager.instance.Save(true);
        SceneManager.LoadScene("MainGame");
        StartCoroutine(InitProfile());
    }
    IEnumerator InitProfile()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        string Jsonstring = //JsonEncrypt.Decrypt(
            File.ReadAllText(Application.persistentDataPath + "/SaveData/BBData.json");
            //, "살려줘...");
        JSONObject saveData = new JSONObject(Jsonstring);
        //Debug.Log(GameManager.instance.playerController.profile.equipStack+", "+ GameManager.instance.playerController.profile.maxEquipStack);

        //유지할 목록
        GameManager.instance.playerController.profile.Gems = BigFloat.Parse(saveData[0]["playerInfo"]["gems"].str);
        GameManager.instance.playerController.profile.isDoneTut = true;
        GameManager.instance.playerController.profile.BombAmount = (int)saveData[0]["playerInfo"]["bombAmount"].i;

        //Debug.Log(GameManager.instance.playerController.profile.equipStack + ", " + GameManager.instance.playerController.profile.maxEquipStack);
        GameManager.instance.textEquipStack.text = "착용한 코스튬: " + (GameManager.instance.playerController.profile.maxEquipStack - GameManager.instance.playerController.profile.equipStack) + "/<color=red><b>" + GameManager.instance.playerController.profile.maxEquipStack + "</b></color>";
        
        DataManager.instance.Save(false);
        SceneManager.LoadScene("MainGame");
        yield return new WaitForEndOfFrame();
        UseHeart();
        DataManager.instance.Load();
        isTimeTraveling = false;
    }

    //타임머신 시간계산

    //게임 초기화, 중간 이탈, 중간 복귀 시 실행되는 함수
    public void OnApplicationFocus(bool value)
    {
        Debug.Log("OnApplicationFocus() : " + value);
        if (value)
        {
            LoadHeartInfo();
            LoadAppQuitTime();
            SetRechargeScheduler();
        }
        else
        {
            SaveHeartInfo();
            SaveAppQuitTime();
            if (supplyTimerCoroutine != null)
            {
                StopCoroutine(supplyTimerCoroutine);
            }
        }
    }
    //게임 종료 시 실행되는 함수
    public void OnApplicationQuit()
    {
        Debug.Log("GoodsRechargeTester: OnApplicationQuit()");
        SaveHeartInfo();
        SaveAppQuitTime();
    }
    //버튼 이벤트에 이 함수를 연동한다.
    public void OnClickUseHeart()
    {
        Debug.Log("OnClickUseHeart");
        UseHeart();
    }

    public void Init()
    {
        IsReadyTimeTravel = 0;
        remainHeartSupplyTime = 0;
        m_AppQuitTime = new DateTime(1970, 1, 1).ToLocalTime();
        Debug.Log("heartRechargeTimer : " + remainHeartSupplyTime + "s");
    }
    public bool LoadHeartInfo()
    {
        Debug.Log("LoadHeartInfo");
        bool result = false;
        try
        {
            if (PlayerPrefs.HasKey("isReadyTimeTravel"))
            {
                Debug.Log("PlayerPrefs has key : isReadyTimeTravel");
                IsReadyTimeTravel = PlayerPrefs.GetInt("isReadyTimeTravel");
                if (IsReadyTimeTravel < 0)
                    IsReadyTimeTravel = 0;
            }
            else
                IsReadyTimeTravel = 1;
            //isReadyTimeTravelLabel.text = playerProfile.heartCount.ToString();
            Debug.Log("Loaded isReadyTimeTravel : " + IsReadyTimeTravel);
            result = true;
        }
        catch (System.Exception e)
        {
            Debug.LogError("LoadHeartInfo Failed (" + e.Message + ")");
        }
        return result;
    }
    public bool SaveHeartInfo()
    {
        Debug.Log("SaveHeartInfo");
        bool result = false;
        try
        {
            PlayerPrefs.SetInt("isReadyTimeTravel", IsReadyTimeTravel);
            PlayerPrefs.Save();
            Debug.Log("Saved isReadyTimeTravel : " + IsReadyTimeTravel);
            result = true;
        }
        catch (System.Exception e)
        {
            Debug.LogError("SaveHeartInfo Failed (" + e.Message + ")");
        }
        return result;
    }
    public bool LoadAppQuitTime()
    {
        Debug.Log("LoadAppQuitTime");
        bool result = false;
        try
        {
            if (PlayerPrefs.HasKey("AppQuitTime"))
            {
                Debug.Log("PlayerPrefs has key : AppQuitTime");
                var appQuitTime = string.Empty;
                appQuitTime = PlayerPrefs.GetString("AppQuitTime");
                m_AppQuitTime = DateTime.FromBinary(Convert.ToInt64(appQuitTime));
            }
            else
            {
                Debug.Log("현재시간 불러옴");
                m_AppQuitTime = new DateTime(1970, 1, 1).ToLocalTime();
            }

            Debug.Log(string.Format("Loaded AppQuitTime : {0}", m_AppQuitTime.ToString()));
            //appQuitTimeLabel.text = string.Format("AppQuitTime : {0}", m_AppQuitTime.ToString());
            result = true;
        }
        catch (System.Exception e)
        {
            Debug.LogError("LoadAppQuitTime Failed (" + e.Message + ")");
        }
        return result;
    }
    public bool SaveAppQuitTime()
    {
        Debug.Log("SaveAppQuitTime");
        bool result = false;
        try
        {
            var appQuitTime = DateTime.Now.ToLocalTime().ToBinary().ToString();
            PlayerPrefs.SetString("AppQuitTime", appQuitTime);
            PlayerPrefs.SetInt("RemainTime", remainHeartSupplyTime);
            PlayerPrefs.Save();
            Debug.Log("Saved AppQuitTime : " + DateTime.Now.ToLocalTime().ToString());
            result = true;
        }
        catch (System.Exception e)
        {
            Debug.LogError("SaveAppQuitTime Failed (" + e.Message + ")");
        }
        return result;
    }
    public void SetRechargeScheduler(Action onFinish = null)
    {
        if (supplyTimerCoroutine != null)
        {
            StopCoroutine(supplyTimerCoroutine);
        }
        var timeDifferenceInSec = (int)((DateTime.Now.ToLocalTime() - m_AppQuitTime).TotalSeconds);
        Debug.Log("TimeDifference In Sec :" + timeDifferenceInSec + "s");
        int origintimeDifferenceInSec = timeDifferenceInSec;

        int remainTime = 0;
        int heartToAdd;
        if (timeDifferenceInSec > 0)
        {

            timeDifferenceInSec = PlayerPrefs.GetInt("RemainTime") - timeDifferenceInSec;

            if (timeDifferenceInSec <= 0)
            {
                isReadyTimeTravel++;
                timeDifferenceInSec = Mathf.Abs(timeDifferenceInSec);
                heartToAdd = timeDifferenceInSec / HeartSupplyInterval;
                Debug.Log("Heart to add : " + heartToAdd);
                if (heartToAdd == 0)
                    remainTime = HeartSupplyInterval - timeDifferenceInSec;
                else
                    remainTime = HeartSupplyInterval - (timeDifferenceInSec % HeartSupplyInterval);
            }
            else
            {
                heartToAdd = timeDifferenceInSec / HeartSupplyInterval;
                Debug.Log("Heart to add : " + heartToAdd);
                if (heartToAdd == 0)
                    remainTime = PlayerPrefs.GetInt("RemainTime") - origintimeDifferenceInSec;
            }
            Debug.Log("RemainTime : " + remainTime);
            isReadyTimeTravel += heartToAdd;
        }
        else if (timeDifferenceInSec < 0)
            Debug.Log("선생님? 시간여행자이십니까?");

        if (IsReadyTimeTravel >= MAX_HEART)
        {
            IsReadyTimeTravel = MAX_HEART;
        }
        else
        {
            supplyTimerCoroutine = StartCoroutine(DoRechargeTimer(remainTime, onFinish));
        }
        //isReadyTimeTravelLabel.text = string.Format("Hearts : {0}", playerProfile.heartCount.ToString());
        Debug.Log("isReadyTimeTravel : " + IsReadyTimeTravel);
    }
    public void UseHeart(Action onFinish = null)
    {
        if (IsReadyTimeTravel <= 0)
        {
            return;
        }

        IsReadyTimeTravel = 0;
        //isReadyTimeTravelLabel.text = string.Format("Hearts : {0}", playerProfile.heartCount.ToString());
        if (supplyTimerCoroutine == null)
        {
            supplyTimerCoroutine = StartCoroutine(DoRechargeTimer(HeartSupplyInterval));
        }
        if (onFinish != null)
        {
            onFinish();
        }
    }
    private IEnumerator DoRechargeTimer(int remainTime, Action onFinish = null)
    {
        Debug.Log("DoRechargeTimer");
        if (remainTime <= 0)
        {
            remainHeartSupplyTime = HeartSupplyInterval;
        }
        else
        {
            remainHeartSupplyTime = remainTime;
        }
        Debug.Log("heartRechargeTimer : " + remainHeartSupplyTime + "s");
        //heartRechargeTimer.text = string.Format("Timer : {0} s",  remainHeartSupplyTime);

        while (remainHeartSupplyTime > 0)
        {
            Debug.Log("heartRechargeTimer : " + remainHeartSupplyTime + "s");
            if (GameManager.instance != null)
            {
                int hour = (int)Math.Truncate((double)remainHeartSupplyTime / 3600);
                int min = (int)Math.Truncate((double)remainHeartSupplyTime % 3600 / 60);
                int sec = (int)(remainHeartSupplyTime % 3600 % 60);
                GameManager.instance.textTimer.text = string.Format("<color=red>{0}{1}{2}</color>\r\n이후 사용가능", hour == 0 ? "" : hour + "시 ", min == 0 ? "" : min + "분 ", sec + "초");
            }
            remainHeartSupplyTime -= 1;
            yield return new WaitForSeconds(1f);
        }
        IsReadyTimeTravel++;
        if (IsReadyTimeTravel >= MAX_HEART)
        {
            IsReadyTimeTravel = MAX_HEART;
            remainHeartSupplyTime = 0;
            //heartRechargeTimer.text = string.Format("Timer : {0} s",  remainHeartSupplyTime);
            Debug.Log("isReadyTimeTravel reached max amount");
            supplyTimerCoroutine = null;
        }
        else
        {
            supplyTimerCoroutine = StartCoroutine(DoRechargeTimer(HeartSupplyInterval, onFinish));
        }
        //isReadyTimeTravelLabel.text = string.Format("Hearts : {0}", playerProfile.heartCount.ToString());
        Debug.Log("isReadyTimeTravel : " + IsReadyTimeTravel);
    }
}
