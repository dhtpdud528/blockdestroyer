﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpendMap : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float size = GameManager.instance.playerController.transform.position.x + 5;
        size = size < 30 ? 30 : size;
        transform.localScale = new Vector3(size, size, 1);
    }
}
