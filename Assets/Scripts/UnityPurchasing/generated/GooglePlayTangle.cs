// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("3yowlKyFeqA/iB34lr1E/ZavQ2RpNLOHGQBx/Ht7ty29H4QJFUtIYwVeOJKqEvWVS+ZjWYMIMqzs6Vv6sQIgDmGWCZ//l9ojEykAqEHycBjnVHz+8yhji8eza8BLdr+HnLra5v/9k4wPYfVpOMgc7YvkGjGIHE36wVpbUiZf9pTjLTHH3pcoNbTgkKrAQ01CcsBDSEDAQ0NC8rzHGiKZa2H7g3n2mxglswwWuY6xjvxM83zAcsBDYHJPREtoxArEtU9DQ0NHQkFJsmaQA/LK9a1efq8vZfiDZppI1avPqtCnTNMBsgf4FkrKddQG1woR8rao9w32wavuLme77jJTiIZqPaX/NeBb8SR/Nr+y+iMOe/gXilSdFEVHW6eIubHdsUBBQ0JD");
        private static int[] order = new int[] { 13,9,8,12,11,6,12,9,12,13,11,11,12,13,14 };
        private static int key = 66;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
