﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;

public class MenuController : MonoBehaviour
{
    public Vector2 firstTouchPosition;
    public Vector2 updateTouchPosition;
    public Transform tapContentsLayout;
    List<GameObject> taps = new List<GameObject>();
    private bool isOver;
    bool isOn;
    public bool IsOn
    {
        get
        {
            return isOn;
        }
        set
        {
            isOn = value;
            if (isOn)
                transform.parent.DOLocalMoveY(900, 0.2f);
            else
                transform.parent.DOLocalMoveY(-10, 0.2f);
        }
    }

    public void SwitchTap(GameObject tap)
    {
        for (int i = 0; i < taps.Count; i++)
            taps[i].transform.localPosition = new Vector2(999, 999);
        tap.transform.localPosition = new Vector2(0, 0);
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        isOver = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isOver = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < tapContentsLayout.childCount; i++)
            taps.Add(tapContentsLayout.GetChild(i).gameObject);
        SwitchTap(taps[0]);
    }

    // Update is called once per frame
    void Update()
    {
        if (isOver && Input.GetMouseButtonDown(0))
            firstTouchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (Input.GetMouseButton(0))
            updateTouchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }
    public void MenuOnOff()
    {
        IsOn = !IsOn;
    }
    public void SetMenuActive(bool val)
    {
        if(val)
            transform.parent.DOLocalMoveY(900, 0.2f);
        else
            transform.parent.DOLocalMoveY(-10, 0.2f);
    }
    
}
