﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Numerics;
using Vector2 = UnityEngine.Vector2;
using System;

public class PetComponent : UIPrice
{
    public GameManager.EffectType type;
    public string addValueString;
    public BigFloat addValue = 0;

    public GameObject pet;
    [Header("- 버튼")]
    public Image ButtonCoinImage;

    public Text infoText;
    public Text nameText;

    bool isPurchased;
    public bool IsPurchased
    {
        get
        {
            return isPurchased;
        }
        set
        {
            if (value && GameManager.instance.playerController.profile.Gems < Price)
            {
                PopUpBoxManager.instance.RewardADBox.gameObject.SetActive(true);
                return;
            }
            isPurchased = value;
            GameManager.instance.playerController.profile.Gems -= Price;
            if (isPurchased)
            {
                ButtonCoinImage.gameObject.SetActive(false);
                priceText.text = "장착하기";
            }
        }
    }
    public void SetIsPurchaed(bool value)
    {
        isPurchased = value;
        if (isPurchased)
        {
            ButtonCoinImage.gameObject.SetActive(false);
            priceText.text = "장착하기";
        }
    }
    bool isEquip;
    public bool IsEquip
    {
        get
        {
            return isEquip;
        }
        set
        {
            if (!isPurchased || isEquip == value) return;

            isEquip = value;
            var val = addValue;
            if (isEquip)
            {
                GameManager.instance.petController = Instantiate(pet).transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<PetController>();
                GameManager.instance.petController.profile += GameManager.instance.tempPetProfile;

                priceText.text = "해재하기";
            }
            else
            {
                //GameManager.instance.petController.transform.parent.parent.parent.gameObject.SetActive(false);
                if (GameManager.instance.petController != null)
                {
                    var prePet = GameManager.instance.petController.transform.parent.parent.parent.gameObject;
                    Destroy(prePet);
                }

                priceText.text = "장착하기";
                val *= -1;
            }
            GameManager.instance.OnStrength(type, ref val);
        }
    }
    public void SetIsEquip(bool value)
    {
        isEquip = value;
        var val = addValue;
        if (isEquip)
        {
            GameManager.instance.petController = Instantiate(pet).transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<PetController>();
            GameManager.instance.petController.profile += GameManager.instance.tempPetProfile;

            priceText.text = "해재하기";
        }
        else
        {
            //GameManager.instance.petController.transform.parent.parent.parent.gameObject.SetActive(false);
            if (GameManager.instance.petController != null)
            {
                var prePet = GameManager.instance.petController.transform.parent.parent.parent.gameObject;
                Destroy(prePet);
            }
            priceText.text = "장착하기";
        }
    }
    public override void Awake()
    {
        base.Awake();
        priceText.text = GameManager.ConvertValue(Price);
        addValue = BigFloat.Parse(addValueString);

        string explain = "";
        switch (type)
        {
            case GameManager.EffectType.CriticalMultiple:
                explain = "치명타\r\n" + (addValue > 1 ? "+" : "") + addValue.ToString(1) + "배";
                break;
            case GameManager.EffectType.CriticalChance:
                explain = "치명타 확률\r\n" + (addValue > 0 ? "+" : "") + addValue.ToString(1) + "%";
                break;
            case GameManager.EffectType.AutoAttackTime:
                explain = "자동 공격 시간\r\n" + (addValue > 0 ? "+" : "") + addValue.ToString(1) + "초";
                break;
            case GameManager.EffectType.AutoSpawnTime:
                explain = "자동 블록 추가 시간\r\n" + (addValue > 0 ? "+" : "") + addValue.ToString(1) + "초";
                break;
            case GameManager.EffectType.Bounciness:
                explain = "탄성\r\n" + (addValue > 0 ? "+" : "") + addValue.ToString(1);
                break;
            case GameManager.EffectType.MoreCoin:
                explain = "추가 코인 획득\r\n" + (addValue > 0 ? "+" : "") + (addValue * 100).ToString(1) + "%";
                break;
            case GameManager.EffectType.PetDamageMultiple:
                explain = "펫 추가 공격력\r\n" + (addValue > 0 ? "+" : "") + (addValue * 100).ToString(1) + "%";
                break;
            case GameManager.EffectType.PetCriticalChance:
                explain = "펫 치명타 확률\r\n" + (addValue > 0 ? "+" : "") + addValue.ToString(1) + "%";
                break;
            case GameManager.EffectType.PetCriticalMultiple:
                explain = "펫 치명타\r\n" + (addValue > 0 ? "+" : "") + addValue.ToString(1) + "배";
                break;
            case GameManager.EffectType.PetSpeed:
                explain = "펫 회전 속도\r\n" + (addValue > 0 ? "+" : "") + addValue.ToString(1);
                break;
            case GameManager.EffectType.AttackDamageMultiple:
                explain = "추가 공격력\r\n" + (addValue > 0 ? "+" : "") + (addValue * 100).ToString(1) + "%";
                break;
        }
        infoText.text = explain;
    }
    public override void UpdateColor()
    {
        if (!isPurchased)
            base.UpdateColor();
    }
    public void OnButtonClick()
    {

        if (!isPurchased)
            IsPurchased = true;
        else
        {
            for (int i = 0; i < transform.parent.childCount; i++)
            {
                if (transform.parent.GetChild(i).GetComponent<PetComponent>().IsPurchased && !transform.parent.GetChild(i).gameObject.Equals(gameObject))
                    transform.parent.GetChild(i).GetComponent<PetComponent>().IsEquip = false;
            }
            IsEquip = !isEquip;
        }
            
    }
}
